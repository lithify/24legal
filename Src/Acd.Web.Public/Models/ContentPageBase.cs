﻿using Lithe.N2Web.Models;
using N2.Details;
using N2.Persistence;
using Lithe.N2Web;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    [AvailableZone("Page Head", "Head")]
    [AvailableZone("Page Head on This and All Child Pages", "HeadRecursive")]
    [WithEditableName("Name", 14, ContainerName = CoreDefaults.Containers.Metadata)]
    public abstract class ContentPageBase : PageModelBase
    {
        /// <summary>
        /// Image used on the page and on listings.
        /// </summary>
        [EditableMediaUpload(PreferredSize = "wide")]
        [Persistable(Length = 256)] // to minimize select+1
        public virtual string Image { get; set; }

        /// <summary>
        /// Title that replaces the regular title when not empty.
        /// </summary>
        [EditableText(Title = "Alternative title", ContainerName = CoreDefaults.Containers.Metadata)]
        public virtual string SeoTitle { get; set; }
        /// <summary>
        /// Summary text displayed in listings.
        /// </summary>
        [EditableSummary(Title = "Summary", SortOrder = 200, Source = "Text", ContainerName = CoreDefaults.Containers.Content)]
        [Persistable(Length = 1024)] // to minimize select+1
        public virtual string Summary { get; set; }

        /// <summary>
        /// Main content of this content item.
        /// </summary>
        [EditableFreeTextArea(SortOrder = 201, ContainerName = CoreDefaults.Containers.Content)]
        [DisplayableTokens]
        public virtual string Text { get; set; }

        /// <summary>
        /// Gets or sets a value that indicates whether this <see cref="ContentPage"/> is visible in the header links.
        /// </summary>
        [EditableCheckBox("Display in header links", 1, ContainerName = CoreDefaults.Containers.Metadata, HelpTitle = "Checking this option displays the page in the header links")]
        public virtual bool VisibleHeader { get; set; }

        /// <summary>
        /// Gets or sets a value that indicates whether this <see cref="ContentPage"/> is visible in the footer links.
        /// </summary>
        [EditableCheckBox("Display in footer links", 2, ContainerName = CoreDefaults.Containers.Metadata, HelpTitle = "Checking this option displays the page in footer links")]
        public virtual bool VisibleFooter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ContentPage"/> should show its page title.
        /// </summary>
        [EditableCheckBox("Show page title", 3, ContainerName = CoreDefaults.Containers.Metadata, DefaultValue = true, HelpTitle = "Unchecking this option hides the page title.")]
        public virtual bool ShowPageTitle { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ContentPage"/> should show its page title.
        /// </summary>
        [EditableCheckBox("Show in breadcrumb", 4, ContainerName = CoreDefaults.Containers.Metadata, DefaultValue = false, HelpTitle = "Checking this option enables a breadcrumb on this page.")]
        public virtual bool ShowBreadcrumb { get; set; }
    }
}