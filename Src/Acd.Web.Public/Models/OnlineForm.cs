﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using N2;
using N2.Details;
using N2.Integrity;
using Lithe.N2Web.Models;

namespace Acd.Web.Public.Models
{
    public partial class OnlineForm
    {
        public SubstituteDecisionMaker SubstituteDecisionMaker1
        {
            get
            {
                var item = SubstituteDecisionMakers.FirstOrDefault();
                return item == null ? new SubstituteDecisionMaker() : item;
            }
        }

        public SubstituteDecisionMaker SubstituteDecisionMaker2
        {
            get
            {
                var item = SubstituteDecisionMakers.Skip(1).FirstOrDefault();
                return item == null ? new SubstituteDecisionMaker() : item;
            }
        }

        public SubstituteDecisionMaker SubstituteDecisionMaker3
        {
            get
            {
                var item = SubstituteDecisionMakers.Skip(2).FirstOrDefault();
                return item == null ? new SubstituteDecisionMaker() : item;
            }
        }

        public string StatusText
        {
            get { return Status == 100 ? "COMPLETED" : "PROCESSING"; }
        }

        public string PdfUrl
        {
            get
            {
                return string.IsNullOrWhiteSpace(PDFSheet)
                    ? PDFSheet
                    : string.Format("{0}://{1}{2}",
                        HttpContext.Current.Request.Url.Scheme,
                        HttpContext.Current.Request.Url.Authority,
                        PDFSheet);
            }
        }
    }
}