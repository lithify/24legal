﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;
using Lithe.N2Web.Models;

namespace Acd.Web.Public.Models
{
    [PartDefinition("Content Links Part")]
    [RestrictChildren(typeof(ContentLink))]
    [WithEditableTitle]
    public class ContentLinksPart : PartModelBase
    {
        [EditableText(Title = "Link Icon Class", SortOrder = 10, DefaultValue = null, Required = true)]
        public virtual string LinkIconClass { get; set; }

        [EditableChildren("ContentLinks", "ContentLinks", 20)]
        public virtual IEnumerable<ContentLink> ContentLinkList { get; set; }

        [PartDefinition("Content Link")]
        [RestrictParents(typeof(ContentLinksPart))]
        [AllowedZones("ContentLinks")]
        public class ContentLink : ContentItem
        {

            [EditableLink(Title = "Link Item", SortOrder = 10, Required = true)]
            public virtual ContentItem LinkItem { get; set; }
        }
    }
}