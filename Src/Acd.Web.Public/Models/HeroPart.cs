﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;
using Lithe.N2Web.Models;

namespace Acd.Web.Public.Models
{
    [PartDefinition("Hero")]
    [RestrictParents(typeof(StartPage))]
    [WithEditableTitle]
    public class HeroPart : PartModelBase
    {
        [EditableImageUpload(Title = "Image", SortOrder = 10, DefaultValue = null, Required = false)]
        public virtual string Image { get; set; }

        [EditableLink(Title = "Link Item", SortOrder = 10, Required = true)]
        public virtual ContentItem LinkItem { get; set; }
    }
}