﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using N2;
using N2.Definitions;
using N2.Details;
using N2.Engine.Globalization;
using N2.Web;
using N2Url = N2.Web.Url;
using Lithe.N2Web;
using N2.Web.UI;

namespace Acd.Web.Public.Models
{
    /// <summary>
    /// This is the start page on a site. Separate start pages can respond to 
    /// a domain name and/or form the root of translation. The registration of
    /// this model is performed by <see cref="Registrations.StartPageRegistration"/>.
    /// </summary>
    [TabContainer("ForgotPasswordEmailTab", "Forgot Password Email", 2100)]
    [TabContainer("ResetPasswordEmailTab", "Reset Password Email", 2200)]
    public class StartPage : ContentPage, IStartPage, ILanguage, ISitesSource
    {

        #region ILanguage Members

        public virtual string LanguageCode { get; set; }

        public string LanguageTitle
        {
            get
            {
                if (string.IsNullOrEmpty(LanguageCode))
                    return "";
                else
                    return new CultureInfo(LanguageCode).DisplayName;
            }
        }

        #endregion

        public virtual string FooterText { get; set; }

        public virtual string Logotype { get; set; }

        [EditableCheckBox("Full menu on homepage", 50, ContainerName = CoreDefaults.Containers.Site)]
        public virtual bool FullMenuOnHomePage
        {
            get;
            set;
        }

        [EditableUrl("Login Page", 79, ContainerName = CoreDefaults.Containers.Site, HelpText = "Page to display when authorization to a page fails.")]
        public virtual string LoginPage
        {
            get { return (string)GetDetail("LoginPage"); }
            set { SetDetail("LoginPage", value); }
        }

        [EditableCheckBox("Require Https", 69, ContainerName = CoreDefaults.Containers.Site)]
        public virtual bool RequireHttps
        {
            get;
            set;
        }

        //[EditableFreeTextArea("Verify Email Template", 1000, ContainerName = CoreDefaults.Containers.Site, HelpText = "Email template for the user to verify the ")]
        //public virtual string VerifyEmailTemplate { get; set; }

        [EditableFileUpload("Homepage Logo", 10)]
        public virtual string HomepageLogo { get; set; }

        #region ISitesSource Members

        public virtual string HostName { get; set; }

        public IEnumerable<Site> GetSites()
        {
            if (!string.IsNullOrEmpty(HostName))
                yield return new Site(Find.EnumerateParents(this, null, true).Last().ID, ID, HostName) { Wildcards = true };
        }

        #endregion

        public virtual string Author { get; set; }
        public virtual string Keywords { get; set; }
        public virtual string Description { get; set; }

        #region Forgot Password Email
        [EditableTextBox("From", 30, ContainerName = "ForgotPasswordEmailTab")]
        public virtual string ForgotPasswordMailFrom { get; set; }
        [EditableTextBox("Subject", 50, ContainerName = "ForgotPasswordEmailTab")]
        public virtual string ForgotPasswordMailSubject { get; set; }
        [EditableFreeTextArea("Body", 100, ContainerName = "ForgotPasswordEmailTab")]
        public virtual string ForgotPasswordMailBody { get; set; }
        #endregion

        #region Reset Password Email
        [EditableTextBox("Subject", 50, ContainerName = "ResetPasswordEmailTab")]
        public virtual string ResetPasswordMailSubject { get; set; }
        [EditableFreeTextArea("Body", 100, ContainerName = "ResetPasswordEmailTab")]
        public virtual string ResetPasswordMailBody { get; set; }
        #endregion
    }
}