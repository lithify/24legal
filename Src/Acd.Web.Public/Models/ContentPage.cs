﻿using System.Web.UI.WebControls;
using N2;
using N2.Definitions;
using N2.Details;
using N2.Persistence;
using Lithe.N2Web;

namespace Acd.Web.Public.Models
{
    /// <summary>
    /// Content page model used in several places:
    ///  * It serves as base class for start page
    ///  * It's the base for "template first" definitions located in /dinamico/default/views/contentpages/
    /// </summary>
    [PageDefinition]
    [WithEditableTemplateSelection(ContainerName = CoreDefaults.Containers.Metadata)]
    public class ContentPage : ContentPageBase, IContentPage, IStructuralPage
    {
        [EditableCheckBox("Show in start page", 10, ContainerName = CoreDefaults.Containers.Metadata, HelpTitle = "Checking this option displays the page in startpage")]
        public virtual bool ShowInStartpage { get; set; }

        [EditableCheckBox("Exclude From Site Map", 5, ContainerName = CoreDefaults.Containers.Metadata, DefaultValue = false, HelpTitle = "Checking this option removes this page from the site map.")]
        public virtual bool ExcludeFromSiteMap { get; set; }
    }
}