﻿using Lithe.N2Web;
using Lithe.N2Web.Models;
using N2;
using N2.Details;

namespace Acd.Web.Public.Models
{
	/// <summary>
	/// This part model is the base of several "template first" definitions 
	/// located in /dinamico/default/views/contentparts/ 
	/// </summary>
	[PartDefinition]
	[WithEditableTemplateSelection(ContainerName = CoreDefaults.Containers.Metadata)]
	public class ContentPart : PartModelBase
	{
	}
}