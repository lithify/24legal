﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using CsvHelper;
using CsvHelper.Configuration;
using N2.Engine;
using N2.Web.Mail;

namespace Acd.Web.Public.Models
{
    [Service(typeof(IOnlineFormService))]
    public class OnlineFormService : IOnlineFormService
    {
        private readonly IMailSender _mailSender;

        public ACDDataContext DataContext { get; set; }
        public ModelStateDictionary ModelState { get; set; }
        public HttpContext HttpContext
        {
            get
            {
                return HttpContext.Current;
            }
        }
        public string Username
        {
            get
            {
                return HttpContext.User.Identity.Name;
            }
        }
        protected string SessionID
        {
            get
            {
                return HttpContext.Session.SessionID;
            }
        }

        public OnlineFormService(IMailSender mailSender)
        {
            _mailSender = mailSender;
        }

        protected OnlineForm GetOnlineForm()
        {
            var onlineForm = from item in DataContext.OnlineForms
                             where item.CreatedBy == Username
                             //&& item.Status != 100
                             select item;

            return onlineForm.FirstOrDefault();
        }

        public OnlineFormModel GetModel()
        {
            var model = new OnlineFormModel
            {
                OnlineForm = GetOnlineForm(),
            };
            return model;
        }

        #region Step 1
        public OnlineFormStep1Model GetStep1Model()
        {
            var model = new OnlineFormStep1Model();

            var entity = GetOnlineForm();
            if (entity != null)
            {
                model.Name = entity.Personal_Name;
                model.Address = entity.Personal_Address;
                model.Phone = entity.Personal_Phone;
                model.DateOfBirth = entity.Personal_DateOfBirth;
            }

            return model;
        }

        public bool ValidateStep1(OnlineFormStep1Model model)
        {
            if (model.DateOfBirth > DateTime.Today.AddYears(-18))
            {
                ModelState.AddModelError("DateOfBirthStr", "You must be 18 or older to complete a valid Advance Care Directive.");
            }
            return ModelState.IsValid;
        }

        public bool SaveStep1(OnlineFormStep1Model model)
        {
            if (ValidateStep1(model))
            {
                var entity = GetOnlineForm();
                if (entity == null)
                {
                    entity = new OnlineForm()
                    {
                        CreatedBy = Username,
                        CreatedAt = DateTime.Now,
                    };
                    DataContext.OnlineForms.InsertOnSubmit(entity);
                }

                entity.Personal_Name = model.Name;
                entity.Personal_Address = model.Address;
                entity.Personal_Phone = model.Phone;
                entity.Personal_DateOfBirth = model.DateOfBirth;
                if (entity.Status < 1)
                {
                    entity.Status = 1;
                }
                entity.UpdatedBy = Username;
                entity.UpdatedAt = DateTime.Now;

                DataContext.SubmitChanges();
            }

            return ModelState.IsValid;
        }
        #endregion

        #region Step 2A
        public OnlineFormStep2AModel GetStep2AModel()
        {
            var model = new OnlineFormStep2AModel();

            var entity = GetOnlineForm();
            if (entity != null)
            {
                model.Name = entity.Personal_Name;
                model.NotCompleteStep2a = entity.NotCompleteStep2a;
                model.NotAppointMaker2 = entity.NotAppointMaker2;
                model.NotAppointMaker3 = entity.NotAppointMaker3;
                model.NotCompleteStep2b = entity.NotCompleteStep2b;
                model.NotCompleteStep3 = entity.NotCompleteStep3;
                model.NotCompleteStep5 = entity.NotCompleteStep5;
                model.Marker1 = new SubstituteDecisionMakerModel(entity.SubstituteDecisionMakers.FirstOrDefault());
                model.Marker2 = new SubstituteDecisionMakerModel(entity.SubstituteDecisionMakers.Skip(1).FirstOrDefault());
                model.Marker3 = new SubstituteDecisionMakerModel(entity.SubstituteDecisionMakers.Skip(2).FirstOrDefault());
            }
            if (model.Marker1 == null) model.Marker1 = new SubstituteDecisionMakerModel();
            if (model.Marker2 == null) model.Marker2 = new SubstituteDecisionMakerModel();
            if (model.Marker3 == null) model.Marker3 = new SubstituteDecisionMakerModel();

            return model;
        }

        public bool ValidateStep2A(OnlineFormStep2AModel model)
        {
            if (model.Marker1 == null) model.Marker1 = new SubstituteDecisionMakerModel();
            if (model.Marker2 == null) model.Marker2 = new SubstituteDecisionMakerModel();
            if (model.Marker3 == null) model.Marker3 = new SubstituteDecisionMakerModel();

            var hasValue1 = ValidateMarker(model.Marker1, "Marker1");
            var hasValue2 = ValidateMarker(model.Marker2, "Marker2");
            var hasValue3 = ValidateMarker(model.Marker3, "Marker3");

            if (!model.NotCompleteStep2a && !hasValue1)
            {
                ModelState.AddModelError("Marker1.Name", "Please fill out the first Substitute Decision-Makers details or select that you do not wish to appoint any.");
            }
            else if (!model.NotAppointMaker2 && !hasValue2)
            {
                ModelState.AddModelError("Marker2.Name", "Please fill out the second Substitute Decision-Makers details or select that you do not wish to appoint another.");
            }
            else if (!model.NotAppointMaker3 && !hasValue3)
            {
                ModelState.AddModelError("Marker3.Name", "Please fill out the third Substitute Decision-Makers details or select that you do not wish to appoint another.");
            }

            return ModelState.IsValid;
        }

        private bool ValidateMarker(SubstituteDecisionMakerModel marker, string prefix)
        {
            var hasValue = (!string.IsNullOrWhiteSpace(marker.Name) ||
                !string.IsNullOrWhiteSpace(marker.Phone) ||
                marker.DateOfBirth != null ||
                !string.IsNullOrWhiteSpace(marker.Name2) ||
                !string.IsNullOrWhiteSpace(marker.Signed) ||
                marker.DateOfBirth != null);

            if (hasValue)
            {
                if (string.IsNullOrWhiteSpace(marker.Name))
                {
                    ModelState.AddModelError(string.Format("{0}.Name", prefix),
                        "The Name field is required.");
                }
                if (string.IsNullOrWhiteSpace(marker.Address))
                {
                    ModelState.AddModelError(string.Format("{0}.Address", prefix),
                        "The Address field is required.");
                }
                if (string.IsNullOrWhiteSpace(marker.Phone))
                {
                    ModelState.AddModelError(string.Format("{0}.Phone", prefix),
                        "The Phone field is required.");
                }
                if (marker.DateOfBirth == null)
                {
                    ModelState.AddModelError(string.Format("{0}.DateOfBirthStr", prefix),
                        "The Date of birth field is required.");
                }
                else if (marker.DateOfBirth != null
                        && marker.DateOfBirth > DateTime.Today.AddYears(-18))
                {
                    ModelState.AddModelError(string.Format("{0}.DateOfBirthStr", prefix),
                        "You must be 18 or older to complete a valid Advance Care Directive.");
                }
            }

            return hasValue;
        }

        public bool SaveStep2A(OnlineFormStep2AModel model)
        {
            if (ValidateStep2A(model))
            {
                var entity = GetOnlineForm();
                var marker1 = entity.SubstituteDecisionMakers.FirstOrDefault();
                var marker2 = entity.SubstituteDecisionMakers.Skip(1).FirstOrDefault();
                var marker3 = entity.SubstituteDecisionMakers.Skip(2).FirstOrDefault();
                FillMarker(entity, model.Marker1, marker1);
                FillMarker(entity, model.Marker2, marker2);
                FillMarker(entity, model.Marker3, marker3);

                entity.NotCompleteStep2a = model.NotCompleteStep2a;
                entity.NotAppointMaker2 = model.NotAppointMaker2;
                entity.NotAppointMaker3 = model.NotAppointMaker3;
                if (entity.Status < 2)
                {
                    entity.Status = 2;
                }
                entity.UpdatedBy = Username;
                entity.UpdatedAt = DateTime.Now;

                DataContext.SubmitChanges();
            }

            return ModelState.IsValid;
        }

        private void FillMarker(OnlineForm onlineForm, SubstituteDecisionMakerModel model, SubstituteDecisionMaker entity)
        {
            if (entity == null)
            {
                entity = new SubstituteDecisionMaker()
                {
                    OnlineForm = onlineForm,
                    CreatedBy = Username,
                    UpdatedAt = DateTime.Now,
                };
                DataContext.SubstituteDecisionMakers.InsertOnSubmit(entity);
            }

            entity.Name = string.Format("{0}", model.Name).ToUpper();
            entity.Address = model.Address;
            entity.Phone = model.Phone;
            entity.DateOfBirth = model.DateOfBirth;
            entity.Name2 = model.Name2;
            entity.Signed = model.Signed;
            entity.Date = model.Date;
            entity.UpdatedBy = Username;
            entity.UpdatedAt = DateTime.Now;
        }
        #endregion

        #region Step 2B
        public OnlineFormStep2BModel GetStep2BModel()
        {
            var model = new OnlineFormStep2BModel();

            var entity = GetOnlineForm();
            if (entity != null)
            {
                model.NotCompleteStep2a = entity.NotCompleteStep2a;
                model.NotAppointMaker2 = entity.NotAppointMaker2;
                model.NotAppointMaker3 = entity.NotAppointMaker3;
                model.NotCompleteStep2b = (entity.NotCompleteStep2a && string.IsNullOrWhiteSpace(entity.ConditionsOfAppointment)) ? true : entity.NotCompleteStep2b;
                model.NotCompleteStep3 = entity.NotCompleteStep3;
                model.NotCompleteStep5 = entity.NotCompleteStep5;
                model.ConditionsOfAppointment = entity.ConditionsOfAppointment;
            }

            return model;
        }

        public bool ValidateStep2B(OnlineFormStep2BModel model)
        {
            if (!model.NotCompleteStep2b &&
                string.IsNullOrWhiteSpace(model.ConditionsOfAppointment))
            {
                ModelState.AddModelError("ConditionsOfAppointment", "Please fill out your conditions, or select not to place any conditions on appointment.");
            }
            return ModelState.IsValid;
        }

        public bool SaveStep2B(OnlineFormStep2BModel model)
        {
            if (ValidateStep2B(model))
            {
                var entity = GetOnlineForm();
                entity.ConditionsOfAppointment = model.ConditionsOfAppointment;
                entity.NotCompleteStep2b = model.NotCompleteStep2b;
                if (entity.Status < 3)
                {
                    entity.Status = 3;
                }
                entity.UpdatedBy = Username;
                entity.UpdatedAt = DateTime.Now;

                DataContext.SubmitChanges();
            }

            return ModelState.IsValid;
        }
        #endregion

        #region Step 3
        public OnlineFormStep3Model GetStep3Model()
        {
            var model = new OnlineFormStep3Model();

            var entity = GetOnlineForm();
            if (entity != null)
            {
                model.NotCompleteStep2a = entity.NotCompleteStep2a;
                model.NotCompleteStep2b = entity.NotCompleteStep2b;
                model.NotCompleteStep3 = entity.NotCompleteStep3;
                model.NotCompleteStep5 = entity.NotCompleteStep5;
                model.WhatIsImportant = entity.WhatIsImportant;
                model.OutcomesOfCareIWishToAvoid = entity.WhatIsImportant_OutcomesOfCareIWishToAvoid;
                model.HealthCareIPrefer = entity.WhatIsImportant_HealthCareIPrefer;
                model.WhereIWishToLive = entity.WhatIsImportant_WhereIWishToLive;
                model.OtherPersonalArrangements = entity.WhatIsImportant_OtherPersonalArrangements;
                model.DyingWishes = entity.WhatIsImportant_DyingWishes;
                //model.NotWriteDownRefusals = entity.NotWriteDownRefusals;
                //model.RefusalsOfHealthCare = entity.RefusalsOfHealthCare;
            }

            return model;
        }

        public bool ValidateStep3(OnlineFormStep3Model model)
        {
            if (!model.NotCompleteStep3 && string.IsNullOrWhiteSpace(model.WhatIsImportant))
            {
                ModelState.AddModelError("WhatIsImportant", "Please complete this section or tick the check box above to state that you do not want to write down what is important to you.");
            }
            //if (!model.NotWriteDownRefusals && string.IsNullOrWhiteSpace(model.RefusalsOfHealthCare))
            //{
            //    ModelState.AddModelError("RefusalsOfHealthCare", "Please complete this section or tick the check box above to state that you do not want to write down any refusals of health care.");
            //}

            return ModelState.IsValid;
        }

        public bool SaveStep3(OnlineFormStep3Model model)
        {
            if (ValidateStep3(model))
            {
                var entity = GetOnlineForm();
                entity.WhatIsImportant = model.WhatIsImportant;
                entity.WhatIsImportant_OutcomesOfCareIWishToAvoid = model.OutcomesOfCareIWishToAvoid;
                entity.WhatIsImportant_HealthCareIPrefer = model.HealthCareIPrefer;
                entity.WhatIsImportant_WhereIWishToLive = model.WhereIWishToLive;
                entity.WhatIsImportant_OtherPersonalArrangements = model.OtherPersonalArrangements;
                entity.WhatIsImportant_DyingWishes = model.DyingWishes;
                //entity.RefusalsOfHealthCare = model.RefusalsOfHealthCare;
                entity.NotCompleteStep3 = model.NotCompleteStep3;
                //entity.NotWriteDownRefusals = model.NotWriteDownRefusals;
                //if (entity.NotCompleteStep3)
                //{
                //    entity.NotWriteDownRefusals = entity.NotCompleteStep3;
                //}
                if (entity.Status < 4)
                {
                    entity.Status = 4;
                }
                entity.UpdatedBy = Username;
                entity.UpdatedAt = DateTime.Now;

                DataContext.SubmitChanges();
            }

            return ModelState.IsValid;
        }
        #endregion

        #region Step 4
        public OnlineFormStep4Model GetStep4Model()
        {
            var model = new OnlineFormStep4Model();

            var entity = GetOnlineForm();
            if (entity != null)
            {
                model.NotCompleteStep2a = entity.NotCompleteStep2a;
                model.NotCompleteStep2b = entity.NotCompleteStep2b;
                model.NotCompleteStep3 = entity.NotCompleteStep3;
                model.NotCompleteStep5 = entity.NotCompleteStep5;
                //model.WhatIsImportant = entity.WhatIsImportant;
                model.NotWriteDownRefusals = entity.NotWriteDownRefusals;
                model.RefusalsOfHealthCare = entity.RefusalsOfHealthCare;
            }

            return model;
        }

        public bool ValidateStep4(OnlineFormStep4Model model)
        {
            //if (!model.NotCompleteStep3 && string.IsNullOrWhiteSpace(model.WhatIsImportant))
            //{
            //    ModelState.AddModelError("WhatIsImportant", "Please complete this section or tick the check box above to state that you do not want to write down what is important to you.");
            //}
            if (!model.NotWriteDownRefusals && string.IsNullOrWhiteSpace(model.RefusalsOfHealthCare))
            {
                ModelState.AddModelError("RefusalsOfHealthCare", "Please complete this section or tick the check box above to state that you do not want to write down any refusals of health care.");
            }

            return ModelState.IsValid;
        }

        public bool SaveStep4(OnlineFormStep4Model model)
        {
            if (ValidateStep4(model))
            {
                var entity = GetOnlineForm();
                //entity.WhatIsImportant = model.WhatIsImportant;
                entity.RefusalsOfHealthCare = model.RefusalsOfHealthCare;
                //entity.NotCompleteStep3 = model.NotCompleteStep3;
                entity.NotWriteDownRefusals = model.NotWriteDownRefusals;
                //if (entity.NotCompleteStep3)
                //{
                //    entity.NotWriteDownRefusals = entity.NotCompleteStep3;
                //}
                if (entity.Status < 5)
                {
                    entity.Status = 5;
                }
                entity.UpdatedBy = Username;
                entity.UpdatedAt = DateTime.Now;

                DataContext.SubmitChanges();
            }

            return ModelState.IsValid;
        }
        #endregion

        #region Step 6
        public OnlineFormStep6Model GetStep6Model()
        {
            var model = new OnlineFormStep6Model();

            var entity = GetOnlineForm();
            if (entity != null)
            {
                model.Name = entity.ACD_Name;
                model.Signed = entity.ACD_Signed;
                model.Date = entity.ACD_Date;
                model.WitnessName = string.Format("{0}", entity.ACD_WitnessName).ToUpper();
                model.PersonName = entity.ACD_PersonName;
                model.Phone = entity.ACD_Phone;
                model.Occupation = entity.ACD_Occupation;
                model.Signed2 = entity.ACD_Signed2;
                model.Date2 = entity.ACD_Date2;
                model.DoNotKnowWitness = entity.DoNotKnowWitness;
                model.AuthorisedWitnessCategory = entity.ACD_AuthorisedWitnessCategory;
                model.SpaceForExtraExecutionStatement = entity.ACD_SpaceForExtraExecutionStatement;

                if (entity.Status < 5 && string.IsNullOrWhiteSpace(model.Name))
                {
                    model.Name = entity.Personal_Name;
                }
            }

            return model;
        }

        public bool ValidateStep6(OnlineFormStep6Model model)
        {
            return ModelState.IsValid;
        }

        public bool SaveStep6(OnlineFormStep6Model model)
        {
            if (ValidateStep6(model))
            {
                var entity = GetOnlineForm();
                entity.ACD_Name = model.Name;
                entity.ACD_Signed = model.Signed;
                entity.ACD_Date = model.Date;
                entity.ACD_WitnessName = model.WitnessName;
                entity.ACD_PersonName = model.PersonName;
                entity.ACD_Phone = model.Phone;
                entity.ACD_Occupation = model.Occupation;
                entity.ACD_Signed2 = model.Signed2;
                entity.ACD_Date2 = model.Date2;
                entity.DoNotKnowWitness = model.DoNotKnowWitness;
                entity.ACD_AuthorisedWitnessCategory = model.AuthorisedWitnessCategory;
                entity.ACD_SpaceForExtraExecutionStatement = model.SpaceForExtraExecutionStatement;

                if (entity.Status < 7)
                {
                    entity.Status = 7;
                }
                entity.UpdatedBy = Username;
                entity.UpdatedAt = DateTime.Now;

                DataContext.SubmitChanges();
            }

            return ModelState.IsValid;
        }
        #endregion

        #region Step 5
        public OnlineFormStep5Model GetStep5Model()
        {
            var model = new OnlineFormStep5Model();

            var entity = GetOnlineForm();
            if (entity != null)
            {
                model.NotCompleteStep2a = entity.NotCompleteStep2a;
                model.NotCompleteStep2b = entity.NotCompleteStep2b;
                model.NotCompleteStep3 = entity.NotCompleteStep3;
                model.NotCompleteStep5 = entity.NotCompleteStep5;
                model.Name = string.Format("{0}", entity.Interpreter_Name).ToUpper();
                model.PersonName = string.Format("{0}", entity.Interpreter_PersonName).ToUpper();
                model.Phone = entity.Interpreter_Phone;
                model.Address = entity.Interpreter_Address;
                model.Signed = entity.Interpreter_Signed;
                model.Date = entity.Interpreter_Date;

                if (entity.Status < 6 && string.IsNullOrWhiteSpace(model.PersonName))
                {
                    model.PersonName = entity.Personal_Name;
                }
            }

            return model;
        }

        public bool ValidateStep5(OnlineFormStep5Model model)
        {
            return ModelState.IsValid;
        }

        public bool SaveStep5(OnlineFormStep5Model model)
        {
            if (ValidateStep5(model))
            {
                var entity = GetOnlineForm();
                entity.Interpreter_Name = model.Name;
                entity.Interpreter_PersonName = model.PersonName;
                entity.Interpreter_Phone = model.Phone;
                entity.Interpreter_Address = model.Address;
                entity.Interpreter_Signed = model.Signed;
                entity.Interpreter_Date = model.Date;

                entity.NotCompleteStep5 = model.NotCompleteStep5;
                if (entity.Status < 6)
                {
                    entity.Status = 6;
                }
                entity.UpdatedBy = Username;
                entity.UpdatedAt = DateTime.Now;

                DataContext.SubmitChanges();
            }

            return ModelState.IsValid;
        }
        #endregion

        #region Review
        public OnlineFormReviewModel GetReviewModel()
        {
            var model = new OnlineFormReviewModel
            {
                OnlineForm = GetOnlineForm(),
            };
            return model;
        }
        #endregion

        #region Complete
        public bool ValidateComplete()
        {
            var entity = GetOnlineForm();

            return entity != null;//&& entity.Status == 6;
        }

        public OnlineFormModel Complete(string from, string adminEmail, string adminSubject, string adminBody, string confirmSubject, string confirmBody)
        {
            var model = GetModel();
            if (model.OnlineForm != null)
            {
                try
                {
                    model.PDFUrl = GeneratePdfSheet(model.OnlineForm);
                    var filename = HttpContext.Server.MapPath(model.PDFUrl);
                    #region Get Size
                    if (File.Exists(filename))
                    {
                        var fileInfo = new FileInfo(filename);
                        if (fileInfo.Length < 1024)
                        {
                            model.PDFSizeDisplay = "1 kb";
                        }
                        else if (fileInfo.Length < 1024 * 1000)
                        {
                            model.PDFSizeDisplay = string.Format("{0:0} kb", fileInfo.Length / 1024);
                        }
                        else
                        {
                            var size = fileInfo.Length / (1024f * 1024);
                            size = Convert.ToInt32(size * 10);
                            size = size / 10;
                            var isize = Convert.ToInt32(size);
                            if (size == isize)
                            {
                                model.PDFSizeDisplay = string.Format("{0:0} mb", size);
                            }
                            else
                            {
                                model.PDFSizeDisplay = string.Format("{0:0.0} mb", size);
                            }
                        }
                    }
                    #endregion

                    #region Set Complete
                    model.OnlineForm.PDFSheet = model.PDFUrl;
                    model.OnlineForm.Status = 100;
                    DataContext.SubmitChanges();
                    #endregion

                    #region Send Mail
                    SendAdminEmail(model, from, adminEmail, adminSubject, adminBody);
                    SendConfirmEmail(model, from, confirmSubject, confirmBody);
                    #endregion
                }
                catch { }
            }
            return model;
        }

        private string GeneratePdfSheet(OnlineForm onlineForm)
        {
            var pdf = new PDFGenerator();
            var url = GetUploadPath();
            url = string.Format("{0}/online-form.pdf", url);
            pdf.FillForm(url, onlineForm);
            return url;
        }

        protected string GetUploadPath()
        {
            ///upload/complaint-forms/{year}/{month}/{day}/{cookie-session-identifier}/
            var url = "/upload/online-forms";
            var folders = new string[]{
                string.Format("{0:yyyy}",DateTime.Today),
                string.Format("{0:MM}",DateTime.Today),
                string.Format("{0:dd}",DateTime.Today),
                SessionID,
            };
            foreach (var folder in folders)
            {
                url = string.Format("{0}/{1}", url, folder);
                var path = HttpContext.Server.MapPath(url);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            return url;
        }
        #endregion

        #region Completed
        public OnlineForm GetLatestCompletedOnlineForm()
        {
            var onlineForm = from item in DataContext.OnlineForms
                             orderby item.UpdatedAt descending
                             where item.CreatedBy == Username
                             && item.Status == 100
                             select item;

            return onlineForm.FirstOrDefault();
        }
        #endregion

        #region Email
        public void SendAdminEmail(OnlineFormModel model, string from, string to, string subject, string body)
        {
            if (!string.IsNullOrWhiteSpace(model.PDFUrl)
                && !string.IsNullOrWhiteSpace(from)
                && !string.IsNullOrWhiteSpace(to))
            {
                var sb = new StringBuilder();
                sb.AppendFormat("<p><a href='{0}://{1}{2}'>{3}</a></p>",
                    HttpContext.Request.Url.Scheme,
                    HttpContext.Request.Url.Authority,
                    model.PDFUrl, "DOWNLOAD PDF");
                body += sb.ToString();

                body = GetHtmlEmail(body, subject);

                SendEmail(from, to, subject, body, new ListDictionary());
            }
        }

        public void SendConfirmEmail(OnlineFormModel model, string from, string subject, string body)
        {
            var user = Membership.GetUser(Username);

            if (user != null
                && !string.IsNullOrWhiteSpace(user.Email)
                && !string.IsNullOrWhiteSpace(model.PDFUrl)
                && !string.IsNullOrWhiteSpace(from)
                && !string.IsNullOrWhiteSpace(body))
            {
                var sb = new StringBuilder();
                sb.AppendFormat("<p><a href='{0}://{1}{2}'>{3}</a></p>",
                    HttpContext.Request.Url.Scheme,
                    HttpContext.Request.Url.Authority,
                    model.PDFUrl, "DOWNLOAD PDF");

                body = GetHtmlEmail(body, subject);
                var replacements = new ListDictionary
                {
                    {"{{first-name}}", model.OnlineForm.Personal_Name},
                    {"{{details}}", sb.ToString()},
                };

                SendEmail(from, user.Email, subject, body, replacements);
            }
        }

        private void SendEmail(string from, string to, string subject, string body, ListDictionary replacements)
        {
            body = GetHtmlEmail(body, subject);

            var md = new MailDefinition()
            {
                From = from,
                Subject = subject,
                IsBodyHtml = true,
            };

            var msg = md.CreateMailMessage(to, replacements, body, new System.Web.UI.Control());

            _mailSender.Send(msg);
        }

        private static string GetHtmlEmail(string emailTemplate, string subject)
        {
            return Resources.EmailBaseTemplate
                .Replace("{{Subject}}", subject)
                .Replace("{{Body}}", emailTemplate);
        }
        #endregion

        public void ExportCsv(Stream stream)
        {
            var outputStream = new StreamWriter(stream);

            var csvWriter = new CsvWriter(outputStream);
            csvWriter.Configuration.RegisterClassMap<OnlineFormMap>();

            var items = DataContext.OnlineForms.OrderByDescending(x => x.UpdatedAt);

            csvWriter.WriteRecords(items);

            outputStream.Flush();
        }
    }

    public sealed class OnlineFormMap : CsvClassMap<OnlineForm>
    {
        public OnlineFormMap()
        {
            Map(m => m.Personal_Name).Name("Name");
            Map(m => m.Personal_Address).Name("Address");
            Map(m => m.Personal_DateOfBirth).Name("Date of birth");

            References<SubstituteDecisionMakerMap>(m => m.SubstituteDecisionMaker1);
            References<SubstituteDecisionMakerMap>(m => m.SubstituteDecisionMaker2);
            References<SubstituteDecisionMakerMap>(m => m.SubstituteDecisionMaker3);
            Map(m => m.ConditionsOfAppointment).Name("Conditions of Appointment");

            Map(m => m.WhatIsImportant).Name("a) When decisions are being made for me, I want people to consider the following");
            Map(m => m.WhatIsImportant_OutcomesOfCareIWishToAvoid).Name("b) Outcomes of care I wish to avoid");
            Map(m => m.WhatIsImportant_HealthCareIPrefer).Name("c) Health care I prefer");
            Map(m => m.WhatIsImportant_WhereIWishToLive).Name("d) here I wish to live");
            Map(m => m.WhatIsImportant_OtherPersonalArrangements).Name("e) ther personal arrangements");
            Map(m => m.WhatIsImportant_DyingWishes).Name("f) Dying wishes");

            Map(m => m.RefusalsOfHealthCare).Name("Binding refusals of health care");

            Map(m => m.Interpreter_Name).Name("Interpreter statement - Name");
            Map(m => m.Interpreter_Phone).Name("Phone");
            Map(m => m.Interpreter_Address).Name("Address");

            Map(m => m.ACD_Name).Name("Witnessing my Advance Care Directive - Name");
            Map(m => m.ACD_WitnessName).Name("Witness statement - Name");
            Map(m => m.ACD_AuthorisedWitnessCategory).Name("Authorised witness category");
            Map(m => m.ACD_Phone).Name("Phone");
            Map(m => m.ACD_SpaceForExtraExecutionStatement).Name("Space for extra execution statement");
           
            Map(m => m.StatusText).Name("Status");
            Map(m => m.PdfUrl).Name("PDF");
            Map(m => m.UpdatedAt).Name("Updated at");
        }
    }

    public sealed class SubstituteDecisionMakerMap : CsvClassMap<SubstituteDecisionMaker>
    {
        public SubstituteDecisionMakerMap()
        {
            Map(m => m.Name).Name("Name");
            Map(m => m.Address).Name("Address");
            Map(m => m.Phone).Name("Phone");
            Map(m => m.DateOfBirth).Name("Date of birth");
        }
    }
}