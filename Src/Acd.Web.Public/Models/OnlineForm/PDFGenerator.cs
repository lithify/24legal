﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using iTextSharp;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace Acd.Web.Public.Models
{
    public class PDFGenerator
    {
        protected string _templateFilename;

        public PDFGenerator()
        {
            _templateFilename = WebContext.Server.MapPath("/upload/online-forms/template.pdf");
        }

        public PDFGenerator(string templateFilename)
        {
            _templateFilename = templateFilename;
        }

        public HttpContext WebContext
        {
            get
            {
                return HttpContext.Current;
            }
        }

        public void FillForm(string fileUrl, OnlineForm onlineForm)
        {
            var filename = WebContext.Server.MapPath(fileUrl);

            var reader = new PdfReader(_templateFilename);

            var stamper = new PdfStamper(reader, new FileStream(filename, FileMode.Create));

            var dic = new Dictionary<string, string>();
            var checkboxes = new List<string>();

            #region Get Values

            #region Part 1: Personal details
            onlineForm.Personal_Name = string.Format("{0}", onlineForm.Personal_Name).ToUpper();
            dic["Personal_Name"] = onlineForm.Personal_Name;
            dic["Personal_Address"] = onlineForm.Personal_Address;
            dic["Personal_Phone"] = onlineForm.Personal_Phone;
            dic["Personal_DateOfBirth_Date"] = string.Format("{0:dd}", onlineForm.Personal_DateOfBirth);
            dic["Personal_DateOfBirth_Month"] = string.Format("{0:MM}", onlineForm.Personal_DateOfBirth);
            dic["Personal_DateOfBirth_Year"] = string.Format("{0:yyyy}", onlineForm.Personal_DateOfBirth);
            #endregion

            #region Part 2a: Appointing Substitute Decision-Makers
            var index = 0;
            foreach (var marker in onlineForm.SubstituteDecisionMakers)
            {
                index++;
                dic[string.Format("Marker{0}_Name", index)] = marker.Name;
                dic[string.Format("Marker{0}_Address", index)] = marker.Address;
                dic[string.Format("Marker{0}_Phone", index)] = marker.Phone;
                dic[string.Format("Marker{0}_DateOfBirth_Date", index)] = string.Format("{0:dd}", marker.DateOfBirth);
                dic[string.Format("Marker{0}_DateOfBirth_Month", index)] = string.Format("{0:MM}", marker.DateOfBirth);
                dic[string.Format("Marker{0}_DateOfBirth_Year", index)] = string.Format("{0:yyyy}", marker.DateOfBirth);
                dic[string.Format("Marker{0}_Name2", index)] = string.IsNullOrWhiteSpace(marker.Name2) ? marker.Name : marker.Name2;
                dic[string.Format("Marker{0}_Signed", index)] = marker.Signed;
                dic[string.Format("Marker{0}_Date_Date", index)] = string.Format("{0:dd}", marker.Date);
                dic[string.Format("Marker{0}_Date_Month", index)] = string.Format("{0:MM}", marker.Date);
                dic[string.Format("Marker{0}_Date_Year", index)] = string.Format("{0:yyyy}", marker.Date);
            }
            #endregion

            //#region Part 2b: Conditions of Appointment
            //dic["ConditionsOfAppointment"] = onlineForm.ConditionsOfAppointment;
            //#endregion

            //#region Part 3: What is important to me – my values and wishes:

            //var field = stamper.AcroFields.GetFieldItem("WhatIsImportant1");

            //var merged = field.GetMerged(0);
            //var textField = new TextField(null, null, null);
            //stamper.AcroFields.DecodeGenericDictionary(merged, textField);
            //var font = textField.Font;
            //var fontSize = textField.FontSize;
            //var fieldDic = field.GetWidget(0);
            //var array = fieldDic.GetAsArray(PdfName.RECT);
            //var width = array.GetAsNumber(2).FloatValue;
            //width -= 160;
            //var height = array.GetAsNumber(3).FloatValue;

            //var rows = new int[] { 45, 46 };

            //var details = string.Format("{0}", onlineForm.WhatIsImportant);

            //var paragraphs = details
            //    .Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            //var lines1 = new List<string>();
            //var lines2 = new List<string>();
            //var lines3 = new List<string>();
            //var lines = lines1;

            //var paraIndex = 0;
            //foreach (var paragraph in paragraphs)
            //{
            //    var words = paragraph.Split(new string[] { " " }, StringSplitOptions.None);

            //    var line = new List<string>();
            //    index = 0;
            //    foreach (var word in words)
            //    {
            //        line.Add(word);
            //        var s = string.Join(" ", line.ToArray());
            //        var w = font.GetWidthPoint(s, fontSize);
            //        if (w > width)
            //        {
            //            var last = line[line.Count - 1];
            //            lines.Add(string.Join(" ", line.Take(line.Count - 1).ToArray()));
            //            index += line.Count - 1;

            //            line = new List<string>();
            //            line.Add(last);
            //        }
            //        switch (paraIndex)
            //        {
            //            case 0:
            //                if (lines1.Count == rows[paraIndex])
            //                {
            //                    lines = lines2;
            //                }
            //                break;
            //            case 1:
            //                if (lines2.Count == rows[paraIndex])
            //                {
            //                    lines = lines3;
            //                }
            //                break;
            //        }
            //    }
            //    if (words.Length > 0 && index < words.Length)
            //    {
            //        lines.Add(string.Join(" ", words.Skip(index).ToArray()));
            //    }

            //    switch (paraIndex)
            //    {
            //        case 0:
            //            if (lines1.Count == rows[paraIndex])
            //            {
            //                lines = lines2;
            //                paraIndex++;
            //            }
            //            break;
            //        case 1:
            //            if (lines2.Count == rows[paraIndex])
            //            }
            //            break;
            //    }
            //}

            //dic["WhatIsImportant1"] = string.Join(Environment.NewLine, lines1.ToArray());
            //dic["WhatIsImportant2"] = string.Join(Environment.NewLine, lines2.ToArray());
            //dic["WhatIsImportant3"] = string.Join(Environment.NewLine, lines3.ToArray());

            //dic["RefusalsOfHealthCare"] = onlineForm.RefusalsOfHealthCare;
            //#endregion {
            //                lines = lines3;
            //                paraIndex++;
            //           

            #region Part 4: Giving my Advance Care Directive
            dic["ACD_Name"] = string.IsNullOrWhiteSpace(onlineForm.ACD_Name) ? onlineForm.Personal_Name : onlineForm.ACD_Name;
            dic["ACD_WItnessName"] = string.Format("{0}", onlineForm.ACD_WitnessName).ToUpper();
            dic["ACD_PersonName"] = string.IsNullOrWhiteSpace(onlineForm.ACD_Name) ? onlineForm.Personal_Name : onlineForm.ACD_Name;
            dic["ACD_Phone"] = onlineForm.ACD_Phone;
            dic["ACD_Occupation"] = onlineForm.ACD_Occupation;
            dic["ACD_AuthorisedWitnessCategory"] = onlineForm.ACD_AuthorisedWitnessCategory;
            #endregion

            #region Part 5: Interpreter statement
            dic["Interpreter_Name"] = string.Format("{0}", onlineForm.Interpreter_Name).ToUpper();
            dic["Interpreter_PersonName"] = string.IsNullOrWhiteSpace(onlineForm.Interpreter_PersonName) ? string.Empty : onlineForm.Interpreter_PersonName;
            dic["Interpreter_Phone"] = onlineForm.Interpreter_Phone;
            dic["Interpreter_Address"] = onlineForm.Interpreter_Address;
            dic["Interpreter_Signed"] = onlineForm.Interpreter_Signed;
            dic["Interpreter_Date_Date"] = string.Format("{0:dd}", onlineForm.Interpreter_Date);
            dic["Interpreter_Date_Month"] = string.Format("{0:MM}", onlineForm.Interpreter_Date);
            dic["Interpreter_Date_Year"] = string.Format("{0:yyyy}", onlineForm.Interpreter_Date);
            #endregion

            #endregion

            #region Did not complete
            if (!onlineForm.NotCompleteStep2a)
            {
                stamper.AcroFields.RemoveField("NotComplete2a");
                stamper.AcroFields.RemoveField("NotComplete2aM");
                stamper.AcroFields.RemoveField("NotComplete2a2");
                stamper.AcroFields.RemoveField("NotComplete2a3");
                for (var i = 1; i < onlineForm.SubstituteDecisionMakers.Count; i++)
                {
                    var maker = onlineForm.SubstituteDecisionMakers[i];
                    if (!string.IsNullOrWhiteSpace(maker.Name))
                    {
                        stamper.AcroFields.RemoveField(string.Format("NotComplete2aMaker{0}", i + 1));
                    }
                }
            }
            else
            {
                stamper.AcroFields.RemoveField("NotComplete2aMaker2");
                stamper.AcroFields.RemoveField("NotComplete2aMaker3");
            }
            if (!onlineForm.NotCompleteStep2b)
            {
                stamper.AcroFields.RemoveField("NotComplete2b");
                stamper.AcroFields.RemoveField("NotComplete2bM");
            }
            if (!onlineForm.NotCompleteStep3)
            {
                stamper.AcroFields.RemoveField("NotComplete3");
                stamper.AcroFields.RemoveField("NotComplete3M");
                stamper.AcroFields.RemoveField("NotComplete3_b");
                stamper.AcroFields.RemoveField("NotComplete3_c");
                stamper.AcroFields.RemoveField("NotComplete3_d");
                stamper.AcroFields.RemoveField("NotComplete3_e");
                stamper.AcroFields.RemoveField("NotComplete3_f");
            }
            if (!onlineForm.NotWriteDownRefusals)
            {
                stamper.AcroFields.RemoveField("NotComplete3b");
                stamper.AcroFields.RemoveField("NotComplete3bM");
            }
            if (!onlineForm.NotCompleteStep5)
            {
                stamper.AcroFields.RemoveField("NotComplete5");
                stamper.AcroFields.RemoveField("NotComplete5M");
            }
            #endregion

            #region Set Values
            foreach (var item in dic)
            {
                stamper.AcroFields.SetField(item.Key, string.Format("{0}", item.Value));
            }
            foreach (var item in checkboxes)
            {
                var states = stamper.AcroFields.GetAppearanceStates(item)
                    .Where(x => string.Compare(x, "Off", true) != 0).ToArray();
                stamper.AcroFields.SetField(item, string.Join(",", states));
            }
            #endregion

            stamper.FormFlattening = true;

            stamper.Close();
            stamper = null;

            reader.Close();
            reader = null;

            FillFormText(filename, onlineForm);
        }

        private void FillFormText(string filename, OnlineForm onlineForm)
        {
            var inputReader = new PdfReader(filename);
            Document inputDoc = new Document(inputReader.GetPageSizeWithRotation(1));

            var outputFilename = filename + ".tmp";
            using (var outputFs = new FileStream(outputFilename, FileMode.Create))
            {
                var outputWriter = PdfWriter.GetInstance(inputDoc, outputFs);
                inputDoc.Open();
                PdfContentByte cb = outputWriter.DirectContent;

                AddPages(inputReader, inputDoc, outputWriter, cb, 1, 2);

                DrawText(inputReader, inputDoc, outputWriter, cb,
                    onlineForm.ConditionsOfAppointment, 3,
                    new Rectangle(142, 100, 560, 210),
                    new Rectangle(142, 105, 560, 720));

                AddPages(inputReader, inputDoc, outputWriter, cb, 4, 4);
                DrawText(inputReader, inputDoc, outputWriter, cb,
                    onlineForm.WhatIsImportant, 4,
                    new Rectangle(142, 106, 560, 715),
                    new Rectangle(142, 106, 560, 715));

                AddPages(inputReader, inputDoc, outputWriter, cb, 5, 5);
                DrawText(inputReader, inputDoc, outputWriter, cb,
                    onlineForm.WhatIsImportant_OutcomesOfCareIWishToAvoid, 5,
                    new Rectangle(142, 106, 560, 715),
                    new Rectangle(142, 106, 560, 715));

                AddPages(inputReader, inputDoc, outputWriter, cb, 6, 6);
                DrawText(inputReader, inputDoc, outputWriter, cb,
                    onlineForm.WhatIsImportant_HealthCareIPrefer, 6,
                    new Rectangle(142, 106, 560, 715),
                    new Rectangle(142, 106, 560, 715));

                AddPages(inputReader, inputDoc, outputWriter, cb, 7, 7);
                DrawText(inputReader, inputDoc, outputWriter, cb,
                    onlineForm.WhatIsImportant_WhereIWishToLive, 7,
                    new Rectangle(142, 106, 560, 715),
                    new Rectangle(142, 106, 560, 715));

                AddPages(inputReader, inputDoc, outputWriter, cb, 8, 8);
                DrawText(inputReader, inputDoc, outputWriter, cb,
                    onlineForm.WhatIsImportant_OtherPersonalArrangements, 8,
                    new Rectangle(142, 106, 560, 715),
                    new Rectangle(142, 106, 560, 715));

                AddPages(inputReader, inputDoc, outputWriter, cb, 9, 9);
                DrawText(inputReader, inputDoc, outputWriter, cb,
                    onlineForm.WhatIsImportant_DyingWishes, 9,
                    new Rectangle(142, 106, 560, 715),
                    new Rectangle(142, 106, 560, 715));

                AddPages(inputReader, inputDoc, outputWriter, cb, 10, 10);

                DrawText(inputReader, inputDoc, outputWriter, cb,
                    onlineForm.RefusalsOfHealthCare, 11,
                    new Rectangle(142, 512, 560, 690),
                    new Rectangle(142, 106, 560, 690));

                AddPages(inputReader, inputDoc, outputWriter, cb, 12, 12);

                DrawText(inputReader, inputDoc, outputWriter, cb,
                    onlineForm.ACD_SpaceForExtraExecutionStatement, 13,
                    new Rectangle(142, 106, 560, 192),
                    new Rectangle(142, 106, 560, 715));

                AddPages(inputReader, inputDoc, outputWriter, cb, 14, 22);

                inputDoc.Close();
            }

            inputDoc.Close();
            inputDoc = null;

            inputReader.Close();
            inputReader = null;

            File.Delete(filename);
            File.Move(outputFilename, filename);
        }

        private void DrawText(PdfReader inputReader, Document inputDoc,
            PdfWriter outputWriter, PdfContentByte cb,
            string text, int pageIndex,
            Rectangle firstRect, Rectangle rect)
        {
            if (string.IsNullOrWhiteSpace(text)) return;

            var baseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, BaseFont.EMBEDDED);
            var font = new Font(baseFont, 12);

            var currentRect = firstRect;

            bool first = true;

            var paragraphs = text.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
            var paragraphList = new List<Paragraph>();
            foreach (var p in paragraphs)
            {
                paragraphList.Add(new Paragraph(p + System.Environment.NewLine, font));
            }

            while (paragraphList.Count > 0)
            {
                if (!first)
                {
                    AddPages(inputReader, inputDoc, outputWriter, cb, pageIndex, pageIndex);
                }

                var col = new ColumnText(cb);
                col.SetSimpleColumn(currentRect);
                var length = 0;
                foreach (var p in paragraphList)
                {
                    col.AddText(p);
                    if (col.Go(true) == ColumnText.NO_MORE_COLUMN)
                    {
                        break;
                    }
                    length++;
                }
                var list = paragraphList.Take(length).ToList();
                var col2 = new ColumnText(cb);
                Phrase ph = new Phrase(30);
                col2.SetSimpleColumn(currentRect);
                ph.AddRange(list);
                col2.SetText(ph);
                col2.Go();

                paragraphList.RemoveRange(0, length);

                first = false;
                currentRect = rect;
            }
        }

        private void AddPages(PdfReader inputReader, Document inputDoc,
            PdfWriter outputWriter, PdfContentByte cb,
            int from, int to)
        {
            for (int i = from; i <= to; i++)
            {
                inputDoc.SetPageSize(inputReader.GetPageSizeWithRotation(i));
                inputDoc.NewPage();

                var page = outputWriter.GetImportedPage(inputReader, i);
                int rotation = inputReader.GetPageRotation(i);

                cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
            }
        }
    }
}