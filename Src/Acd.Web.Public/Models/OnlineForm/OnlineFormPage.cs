﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Acd.Web.Public.Editors;
using Lithe.N2Web;
using N2;
using N2.Details;
using N2.Web.UI;

namespace Acd.Web.Public.Models
{
    [PageDefinition("Online Form", SortOrder = 200)]
    [TabContainer(OnlineFormPage.IntroTab, "Intro", 1005)]
    [TabContainer(OnlineFormPage.Step1Tab, "Step 1", 1010)]
    [TabContainer(OnlineFormPage.Step2aTab, "Step 2a", 1020)]
    [TabContainer(OnlineFormPage.Step2bTab, "Step 2b", 1030)]
    [TabContainer(OnlineFormPage.Step4Tab, "Step 4", 1040)]
    [TabContainer(OnlineFormPage.Step5Tab, "Step 5", 1050)]
    [TabContainer(OnlineFormPage.Step6Tab, "Step 6", 1060)]
    [TabContainer(OnlineFormPage.ProgressTab, "Progress", 1080)]
    [TabContainer(OnlineFormPage.CompleteTab, "Complete", 1100)]
    [TabContainer(OnlineFormPage.AdminTab, "Admin email", 1200)]
    [TabContainer(OnlineFormPage.ConfirmTab, "Confirm email", 1300)]
    public partial class OnlineFormPage : ContentPage
    {
        public const string IntroTab = "IntroTab";
        public const string Step1Tab = "Step1Tab";
        public const string Step2aTab = "Step2aTab";
        public const string Step2bTab = "Step2bTab";
        public const string Step4Tab = "NewStep4Tab";
        public const string Step5Tab = "Step4Tab";
        public const string Step6Tab = "Step5Tab";
        public const string ProgressTab = "ProgressTab";
        public const string CompleteTab = "CompleteTab";
        public const string AdminTab = "AdminTab";
        public const string ConfirmTab = "ConfirmTab";

        [EditableFormEntriesLink("", 30, ContainerName = CoreDefaults.Containers.Content)]
        public virtual string ExportLink { get; set; }

        #region Intro
        [EditableFreeTextArea("Text", 100, ContainerName = IntroTab)]
        public virtual string IntroText { get; set; }
        #endregion

        #region Step 1
        [EditableFreeTextArea("Text", 100, ContainerName = Step1Tab)]
        public virtual string Step1Text { get; set; }
        #endregion

        #region Step 2a
        [EditableFreeTextArea("Text", 100, ContainerName = Step2aTab)]
        public virtual string Step2aText { get; set; }
        #endregion

        #region Step 2b
        [EditableFreeTextArea("Text", 100, ContainerName = Step2bTab)]
        public virtual string Step2bText { get; set; }
        #endregion

        #region Step 4
        [EditableFreeTextArea("Text", 100, ContainerName = Step4Tab)]
        public virtual string NewStep4Text { get; set; }
        #endregion

        #region Step 5
        [EditableFreeTextArea("Text", 100, ContainerName = Step5Tab)]
        public virtual string Step4Text { get; set; }
        #endregion

        #region Step 6
        [EditableFreeTextArea("Text", 100, ContainerName = Step6Tab)]
        public virtual string Step5Text { get; set; }
        #endregion

        #region Progress
        [EditableFreeTextArea("Text", 100, ContainerName = ProgressTab)]
        public virtual string ProgressText { get; set; }
        #endregion

        #region Complete Tab
        [EditableFreeTextArea(Title = "Complete text", SortOrder = 201, ContainerName = OnlineFormPage.CompleteTab)]
        [DisplayableTokens]
        public virtual string CompleteText { get; set; }
        #endregion

        #region Admin email
        [EditableTextBox("Mail from", 30, ContainerName = AdminTab)]
        public virtual string AdminMailFrom { get; set; }
        [EditableTextBox("Mail to", 40, ContainerName = AdminTab)]
        public virtual string AdminMailTo { get; set; }
        [EditableTextBox("Mail subject", 50, ContainerName = AdminTab)]
        public virtual string AdminMailSubject { get; set; }
        [EditableFreeTextArea("Mail body", 100, ContainerName = AdminTab)]
        public virtual string AdminMailBody { get; set; }
        #endregion

        #region Confirm email
        [EditableTextBox("Mail subject", 50, ContainerName = ConfirmTab)]
        public virtual string ConfirmMailSubject { get; set; }
        [EditableFreeTextArea("Mail body", 100, ContainerName = ConfirmTab)]
        public virtual string ConfirmMailBody { get; set; }
        #endregion
    }
}