﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Acd.Web.Public.Models
{
    public abstract class OnlineFormBaseModel
    {
        public OnlineFormPage OnlineFormPage { get; set; }
        public bool GoNext { get; set; }

        [Display(Name = "I do not wish to appoint any Substitute Decision-Makers.")]
        public bool NotCompleteStep2a { get; set; }
        [Display(Name = "I do not want to appoint another Substitute Decision-Maker.")]
        public bool NotAppointMaker2 { get; set; }
        [Display(Name = "I do not want to appoint another Substitute Decision-Maker.")]
        public bool NotAppointMaker3 { get; set; }
        [Display(Name = "I do not want to put conditions on my Substitute Decision-Maker(s).")]
        public bool NotCompleteStep2b { get; set; }
        [Display(Name = "I do not want to write down what is important to me.")]
        public bool NotCompleteStep3 { get; set; }
        [Display(Name = "I did not use an interpreter.")]
        public bool NotCompleteStep5 { get; set; }
    }

    public class OnlineFormModel : OnlineFormBaseModel
    {
        private string[] indexes = new string[]{
            "1",
            "2a",
            "2b",
            "3",
            "4",
            "5",
            "6",
        };
        private string[] texts = new string[]{
            "Personal Details",
            "Appointing Substitute Decision-Makers",
            "Conditions of Appointment",
            "What is important to me – my values and wishes",
            "Binding refusals of health care",
            "Interpreter statement",
            "Giving my Advance Care Directive",
        };
        private string[] urls = new string[]{
            "Step1",
            "Step2A",
            "Step2B",
            "Step3",
            "Step4",
            "Step5",
            "Step6",
        };

        public OnlineForm OnlineForm { get; set; }
        public string PDFUrl { get; set; }
        public string PDFSizeDisplay { get; set; }

        public string[] Indexes
        {
            get { return indexes; }
        }
        public string[] Texts
        {
            get { return texts; }
        }
        public string[] Urls
        {
            get { return urls; }
        }
        public int CurrentStep { get; set; }
    }

    public class OnlineFormStep1Model : OnlineFormBaseModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Date of birth")]
        public string DateOfBirthStr { get; set; }
        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Required]
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        public DateTime? DateOfBirth
        {
            get
            {
                return string.IsNullOrWhiteSpace(DateOfBirthStr) ? null : (DateTime?)Convert.ToDateTime(DateOfBirthStr);
            }
            set
            {
                if (value != null)
                {
                    DateOfBirthStr = string.Format("{0:dd-MM-yyyy}", value);
                }
            }
        }
    }

    public class OnlineFormStep2AModel : OnlineFormBaseModel
    {
        public string Name { get; set; }
        public SubstituteDecisionMakerModel Marker1 { get; set; }
        public SubstituteDecisionMakerModel Marker2 { get; set; }
        public SubstituteDecisionMakerModel Marker3 { get; set; }
    }

    public class SubstituteDecisionMakerModel
    {
        public virtual string Prefix { get; set; }
        public virtual string Person_Name { get; set; }

        public virtual int ID { get; set; }
        [Display(Name = "Name")]
        public virtual string Name { get; set; }
        [Display(Name = "Address")]
        public virtual string Address { get; set; }
        [Display(Name = "Phone")]
        public virtual string Phone { get; set; }
        [Display(Name = "Date of birth")]
        public string DateOfBirthStr { get; set; }

        public DateTime? DateOfBirth
        {
            get
            {
                return string.IsNullOrWhiteSpace(DateOfBirthStr) ? null : (DateTime?)Convert.ToDateTime(DateOfBirthStr);
            }
            set
            {
                if (value != null)
                {
                    DateOfBirthStr = string.Format("{0:dd-MM-yyyy}", value);
                }
            }
        }

        [Display(Name = "Name")]
        public virtual string Name2 { get; set; }
        [Display(Name = "Signed")]
        public virtual string Signed { get; set; }
        [Display(Name = "Date")]
        public virtual DateTime? Date { get; set; }

        public SubstituteDecisionMakerModel()
        {

        }

        public SubstituteDecisionMakerModel(SubstituteDecisionMaker entity)
        {
            if (entity != null)
            {
                ID = entity.ID;
                Name = entity.Name;
                Address = entity.Address;
                Phone = entity.Phone;
                DateOfBirth = entity.DateOfBirth;
                Name2 = entity.Name2;
                Signed = entity.Signed;
                Date = entity.Date;
            }
        }
    }

    public class OnlineFormStep2BModel : OnlineFormBaseModel
    {
        public virtual string ConditionsOfAppointment { get; set; }
    }

    public class OnlineFormStep3Model : OnlineFormBaseModel
    {
        public virtual string WhatIsImportant { get; set; }
        public virtual string OutcomesOfCareIWishToAvoid { get; set; }
        public virtual string HealthCareIPrefer { get; set; }
        public virtual string WhereIWishToLive { get; set; }
        public virtual string OtherPersonalArrangements { get; set; }
        public virtual string DyingWishes { get; set; }
        //public virtual string RefusalsOfHealthCare { get; set; }

        //[Display(Name = "I do not want to write down any refusals of health care.")]
        //public virtual bool NotWriteDownRefusals { get; set; }
    }

    public class OnlineFormStep4Model : OnlineFormBaseModel
    {
        //public virtual string WhatIsImportant { get; set; }
        public virtual string RefusalsOfHealthCare { get; set; }

        [Display(Name = "I do not want to write down any refusals of health care.")]
        public virtual bool NotWriteDownRefusals { get; set; }
    }

    public class OnlineFormStep6Model : OnlineFormBaseModel
    {
        [Display(Name = "Name")]
        public virtual string Name { get; set; }
        [Display(Name = "Signed")]
        public virtual string Signed { get; set; }
        [Display(Name = "Date")]
        public virtual DateTime? Date { get; set; }
        [Display(Name = "Full name of Witness")]
        //[Required]
        public virtual string WitnessName { get; set; }
        [Display(Name = "Name")]
        public virtual string PersonName { get; set; }
        [Display(Name = "Phone")]
        //[Required]
        public virtual string Phone { get; set; }
        [Display(Name = "Occupation of Witness")]
        //[Required]
        public virtual string Occupation { get; set; }
        [Display(Name = "Signed")]
        public virtual string Signed2 { get; set; }
        [Display(Name = "Date")]
        public virtual DateTime? Date2 { get; set; }

        [Display(Name = "I do not know who my witness is at this time.")]
        public virtual bool DoNotKnowWitness { get; set; }

        [Display(Name = "Authorised witness category")]
        public virtual string AuthorisedWitnessCategory { get; set; }
        [Display(Name = "Space for extra execution statement:")]
        public virtual string SpaceForExtraExecutionStatement { get; set; }
    }

    public class OnlineFormStep5Model : OnlineFormBaseModel
    {
        [Display(Name = "Name")]
        public virtual string Name { get; set; }
        [Display(Name = "Name of person giving Advance Care Directive")]
        public virtual string PersonName { get; set; }
        [Display(Name = "Phone")]
        public virtual string Phone { get; set; }
        [Display(Name = "Address")]
        public virtual string Address { get; set; }
        [Display(Name = "Signed")]
        public virtual string Signed { get; set; }
        [Display(Name = "Date")]
        public virtual DateTime? Date { get; set; }
    }

    public class OnlineFormReviewModel : OnlineFormModel
    {
        [Display(Name = "I have reviewed the information and am ready to view my completed Advance Care Directive Form.")]
        [Required]
        public virtual bool Reviewed { get; set; }
    }
}