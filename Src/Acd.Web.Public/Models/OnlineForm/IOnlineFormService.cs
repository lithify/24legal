﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Acd.Web.Public.Models
{
    public interface IOnlineFormService
    {
        ACDDataContext DataContext { get; set; }
        ModelStateDictionary ModelState { get; set; }

        OnlineFormModel GetModel();

        #region Step 1
        OnlineFormStep1Model GetStep1Model();
        bool ValidateStep1(OnlineFormStep1Model model);
        bool SaveStep1(OnlineFormStep1Model model);
        #endregion

        #region Step 2A
        OnlineFormStep2AModel GetStep2AModel();
        bool ValidateStep2A(OnlineFormStep2AModel model);
        bool SaveStep2A(OnlineFormStep2AModel model);
        #endregion

        #region Step 2B
        OnlineFormStep2BModel GetStep2BModel();
        bool ValidateStep2B(OnlineFormStep2BModel model);
        bool SaveStep2B(OnlineFormStep2BModel model);
        #endregion

        #region Step 3
        OnlineFormStep3Model GetStep3Model();
        bool ValidateStep3(OnlineFormStep3Model model);
        bool SaveStep3(OnlineFormStep3Model model);
        #endregion

        #region Step 4
        OnlineFormStep4Model GetStep4Model();
        bool ValidateStep4(OnlineFormStep4Model model);
        bool SaveStep4(OnlineFormStep4Model model);
        #endregion

        #region Step 6
        OnlineFormStep6Model GetStep6Model();
        bool ValidateStep6(OnlineFormStep6Model model);
        bool SaveStep6(OnlineFormStep6Model model);
        #endregion

        #region Step 5
        OnlineFormStep5Model GetStep5Model();
        bool ValidateStep5(OnlineFormStep5Model model);
        bool SaveStep5(OnlineFormStep5Model model);
        #endregion

        #region Review
        OnlineFormReviewModel GetReviewModel();
        #endregion

        #region Complete
        bool ValidateComplete();
        OnlineFormModel Complete(string from, string adminEmail, string adminSubject, string adminBody, string confirmSubject, string confirmBody);
        #endregion

        #region Completed
        OnlineForm GetLatestCompletedOnlineForm();
        #endregion

        #region Email
        void SendAdminEmail(OnlineFormModel model, string from, string to, string subject, string body);
        void SendConfirmEmail(OnlineFormModel model, string from, string subject, string body);
        #endregion

        void ExportCsv(Stream stream);
    }
}