﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    [PartDefinition("Required", SortOrder = 10)]
    public class DynamicFormRequiredValidator : DynamicFormValidatorBase
    {
        public override string GetAttributes()
        {
            return string.Format("data-validation-required='required' data-required-msg='{0}'", Title);
        }
    }
}