﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    [PartDefinition("Text Area", SortOrder = 90)]
    public class DynamicFormTextArea : DynamicFormElementBase
    {
        public override string TemplateKey
        {
            get { return "TextArea"; }
        }
    }
}