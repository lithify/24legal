﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    [PartDefinition("Text Box", SortOrder = 10)]
    public class DynamicFormTextBox : DynamicFormElementBase
    {
        public override string TemplateKey
        {
            get { return "TextBox"; }
        }
    }
}