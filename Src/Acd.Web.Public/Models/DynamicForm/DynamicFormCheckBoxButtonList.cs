﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    [PartDefinition("Check Box List", SortOrder = 50)]
    public class DynamicFormCheckBoxButtonList : DynamicFormOptions
    {
        public override string TemplateKey
        {
            get { return "CheckBoxButtonList"; }
        }
    }
}