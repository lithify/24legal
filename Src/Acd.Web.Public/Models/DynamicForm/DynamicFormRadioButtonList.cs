﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    [PartDefinition("Radio Button List", SortOrder = 55)]
    public class DynamicFormRadioButtonList : DynamicFormOptions
    {
        public override string TemplateKey
        {
            get { return "RadioButtonList"; }
        }
    }
}