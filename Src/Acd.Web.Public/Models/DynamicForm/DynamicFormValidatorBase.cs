﻿using System;
using System.Collections.Generic;
using Lithe.N2Web.Models;
using N2;
using N2.Details;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    [WithEditableTitle("Message", 100)]
    [AllowedZones(DynamicForm.ValidatorsZone)]
    [RestrictParents(typeof(DynamicFormElementBase))]
    public abstract class DynamicFormValidatorBase : PartModelBase, IDynamicFormElement
    {
        public virtual string GetAttributes()
        {
            throw new NotImplementedException();
        }
    }
}