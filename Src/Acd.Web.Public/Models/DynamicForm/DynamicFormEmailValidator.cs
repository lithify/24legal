﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    [PartDefinition("Email", SortOrder = 20)]
    public class DynamicFormEmailValidator : DynamicFormValidatorBase
    {
        public override string GetAttributes()
        {
            return string.Format("data-validation-email='email' data-email-msg='{0}'", Title);
        }
    }
}