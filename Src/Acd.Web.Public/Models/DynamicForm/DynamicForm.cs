﻿using System.Collections.Generic;
using Lithe.N2Web.Models;
using N2;
using N2.Details;
using N2.Integrity;
using N2.Web.UI;

namespace Acd.Web.Public.Models
{
    [PartDefinition("Dynamic Form", IconUrl = "{IconsUrl}/award_star_gold_1.png")]
    [WithEditableTitle(ContainerName = DynamicForm.ContentTab)]
    [TabContainer(DynamicForm.ContentTab, "Content ", 10, CssClass = "tabPanel formTab")]
    [TabContainer(DynamicForm.FieldsTab, "Fields", 40, CssClass = "tabPanel formTab")]
    [TabContainer(DynamicForm.AdminEmailTab, "Admin Email", 70, CssClass = "tabPanel formTab")]
    [TabContainer(DynamicForm.ConfirmEmailTab, "Confirm Email", 80, CssClass = "tabPanel formTab")]
    public class DynamicForm : PartModelBase
    {
        public const string FieldsZone = "FormFields";
        public const string ValidatorsZone = "FormValidators";
        public const string ContentTab = "ContentTab";
        public const string FieldsTab = "FieldsTab";
        public const string AdminEmailTab = "AdminEmailTab";
        public const string ConfirmEmailTab = "ConfirmEmailTab";

        public override string TemplateKey
        {
            get { return "DynamicForm"; }
        }

        #region Content Tab
        [EditableTextBox("First Name Key", 20, ContainerName = DynamicForm.ContentTab,
            HelpTitle = "Seek the first name by the key.")]
        public virtual string FirstNameKey { get; set; }

        [EditableTextBox("Last Name Key", 30, ContainerName = DynamicForm.ContentTab,
            HelpTitle = "Seek the last name by the key.")]
        public virtual string LastNameKey { get; set; }

        [EditableTextBox("Email Key", 40, ContainerName = DynamicForm.ContentTab,
            HelpTitle = "Seek the email address by the key.")]
        public virtual string EmailKey { get; set; }

        [EditableTextBox("Submit Button Text", 50, ContainerName = DynamicForm.ContentTab,
            HelpTitle = "The submit button's text")]
        public virtual string SubmitButtonText { get; set; }

        [EditableUrl("Submit Page", 80, ContainerName = DynamicForm.ContentTab,
            HelpTitle = "Redirects to this page after the form is submitted.")]
        public virtual string SubmitPage { get; set; }

        //[EditableFreeTextArea("Text", 200, ContainerName = DynamicForm.ContentTab)]
        //public virtual string Text { get; set; }
        #endregion

        #region Fields Tab
        [EditableCheckBox("Show Recaptcha", 8, ContainerName = FieldsTab, HelpTitle = "Show reCaptcha")]
        public virtual bool ShowReCaptcha
        {
            get { return base.GetDetail<bool>("ShowReCaptcha", false); }
            set { base.SetDetail("ShowReCaptcha", value); }
        }

        [EditableChildren("Fields", DynamicForm.FieldsZone, 100, ContainerName = DynamicForm.FieldsTab)]
        public virtual IEnumerable<DynamicFormElementBase> FormFields { get; set; }
        #endregion

        #region Admin Email
        [EditableTextBox("From", 20, ContainerName = DynamicForm.AdminEmailTab)]
        public virtual string AdminEmailFrom { get; set; }

        [EditableTextBox("To", 30, ContainerName = DynamicForm.AdminEmailTab)]
        public virtual string AdminEmailTo { get; set; }

        [EditableTextBox("Subject", 40, ContainerName = DynamicForm.AdminEmailTab)]
        public virtual string AdminEmailSubject { get; set; }

        [EditableFreeTextArea("Body", 150, ContainerName = DynamicForm.AdminEmailTab)]
        public virtual string AdminEmailBody { get; set; }
        #endregion

        #region Admin Email
        [EditableTextBox("Subject", 40, ContainerName = DynamicForm.ConfirmEmailTab)]
        public virtual string ConfirmEmailSubject { get; set; }

        [EditableFreeTextArea("Body", 150, ContainerName = DynamicForm.ConfirmEmailTab)]
        public virtual string ConfirmEmailBody { get; set; }
        #endregion
    }
}