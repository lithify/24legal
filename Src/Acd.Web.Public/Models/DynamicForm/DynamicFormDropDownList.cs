﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    [PartDefinition("DropDown List", SortOrder = 45)]
    public class DynamicFormDropDownList : DynamicFormOptions
    {
        public override string TemplateKey
        {
            get { return "DropDownList"; }
        }
    }
}