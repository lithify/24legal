﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;
using System.Linq;
using System.Web.Mvc;
using Lithe.N2Web.Models;

namespace Acd.Web.Public.Models
{
    [WithEditableTitle]
    [AllowedZones(DynamicForm.FieldsZone)]
    [RestrictParents(typeof(DynamicForm))]
    public abstract class DynamicFormElementBase : PartModelBase, IDynamicFormElement
    {
        public virtual string ElementName
        {
            get { return string.Format("input_{0}", ID); }
        }

        [EditableTextBox("Field Key", 20, HelpTitle = "Seek a value of a field by the field key.")]
        public virtual string FieldKey { get; set; }

        [EditableChildren("Validators", DynamicForm.ValidatorsZone, 100)]
        public virtual IEnumerable<DynamicFormValidatorBase> Validators { get; set; }

        public virtual bool Required
        {
            get
            {
                return Validators.Count(x => x is DynamicFormRequiredValidator) > 0;
            }
        }

        public virtual bool HasFormValue
        {
            get { return true; }
        }

        public virtual string GetAttributes()
        {
            var list = from item in Validators
                       select item.GetAttributes();

            return string.Join(" ", list.ToArray());
        }

        public virtual string GetValue(FormCollection form)
        {
            return form[ElementName];
        }
    }
}