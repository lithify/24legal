﻿using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    [PartDefinition("File", SortOrder = 80)]
    public class DynamicFormFile : DynamicFormElementBase
    {
        public override string TemplateKey
        {
            get { return "File"; }
        }
    }
}