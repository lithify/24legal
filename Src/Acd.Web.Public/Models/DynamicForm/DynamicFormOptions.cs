﻿using System;
using System.Collections.Generic;
using N2;
using N2.Details;
using N2.Integrity;

namespace Acd.Web.Public.Models
{
    public abstract class DynamicFormOptions : DynamicFormElementBase
    {
        [EditableTextBox("Options", 50, Rows = 6, TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine)]
        public virtual string OptionsText { get; set; }

        public virtual List<string> Options
        {
            get
            {
                var s = string.Format("{0}", OptionsText);
                if (string.IsNullOrWhiteSpace(s))
                {
                    return new List<string>();
                }

                return new List<string>(s.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries));
            }
        }
    }
}