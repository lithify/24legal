﻿/*
	Create a job on the database to clean the expired online form data.
*/
declare @tran nvarchar(50)
declare @days int
set @days=30
set @tran='DELETE'

begin try
	begin tran @tran
	delete from SubstituteDecisionMaker where OnlineFormID in (select ID from OnlineForm where DATEDIFF(d,updatedat,getdate())>=@days)

	delete from OnlineForm where DATEDIFF(d,updatedat,getdate())>=@days

	commit tran @tran
end try
begin catch
	rollback tran @tran
end catch