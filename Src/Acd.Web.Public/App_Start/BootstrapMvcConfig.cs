﻿[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Lithe.N2Web.App_Start.BootstrapMvcConfig), "Start")]

namespace Lithe.N2Web.App_Start
{
    public static class BootstrapMvcConfig
    { /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            TwitterBootstrapMVC.Bootstrap.Configure();
        }
    }
}