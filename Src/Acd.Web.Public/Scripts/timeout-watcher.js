﻿(function ($) {
    if (!window.session) {
        window.session = {
            monitorAuthenticationTimeout: function (redirectUrl, pingUrl, warningDuration, cushion) {

                // If params not specified, use defaults.
                redirectUrl = redirectUrl || "/membership/logon";
                pingUrl = pingUrl || "/membership/ping";
                warningDuration = warningDuration || 30*1000;//45000;
                cushion = cushion || 4000;

                var timeoutStartTime,
                    timeout,
                    timer,
                    popup,
                    countdown,
                    pinging;

                var updateCountDown = function () {
                    var secondsRemaining = Math.floor((timeout - ((new Date()).getTime() - timeoutStartTime)) / 1000),
                        min = Math.floor(secondsRemaining / 60),
                        sec = secondsRemaining % 60;

                    countdown.text((min > 0 ? min + ":" : "") + (sec < 10 ? "0" + sec : sec));

                    // If timeout hasn't expired, continue countdown.
                    if (secondsRemaining > 0) {
                        timer = window.setTimeout(updateCountDown, 1000);
                    }
                        // Else redirect to login.
                    else {
                        window.location = redirectUrl;
                    }
                };

                var showWarning = function () {
                    if (!$('#timeout-modal').length) {
                        popup = $(
                        "<div id=\"timeout-modal\" class=\"modal fade\">" +
                          "<div class=\"modal-dialog\">" +
                            "<div class=\"modal-content\">" +
                              "<div class=\"modal-header\">" +
                                "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                                "<h4 class=\"modal-title\">Session timeout warning</h4>" +
                              "</div>" +
                              "<div class=\"modal-body\">" +
                                "<p>Your sessions is about to expire, any unsaved changes will be lost.</p>" +
                                "<p>Click the continue button to keep your session active.</p>" +
                              "</div>" +
                              "<div class=\"modal-footer\">" +
                                "<button id=\"continueBtn\" type=\"button\" class=\"btn btn-primary\">Continue</button>" +
                              "</div>" +
                            "</div>" +
                          "</div>" +
                        "</div>").appendTo($("body"));
                        countdown = popup.find("#countDown");

                        $('#continueBtn').on('click', function() {
                            resetTimeout();
                            popup.modal("hide");
                        });
                    }

                    popup.modal("show");
                    updateCountDown();
                };
                var resetTimeout = function () {
                    // Reset timeout by "pinging" server.
                    if (!pinging) {
                        pinging = true;
                        var pingTime = (new Date()).getTime();
                        $.ajax({
                            type: "GET",
                            dataType: "json",
                            url: pingUrl,
                        }).success(function (result) {

                            // Stop countdown.
                            window.clearTimeout(timer);
                            if (popup) {
                                popup.modal('hide');
                            }

                            // Subract time it took to do the ping from
                            // the returned timeout and a little bit of
                            // cushion so that client will be logged out
                            // just before timeout has expired.
                            timeoutStartTime = (new Date()).getTime();
                            timeout = /*result.timeout */ 40000 - (timeoutStartTime - pingTime) - cushion;

                            // Start warning timer.
                            timer = window.setTimeout(showWarning, timeout - warningDuration);
                            pinging = false;
                        });
                    }
                };

                // If user interacts with browser, reset timeout.
                //$(document).on("mousedown mouseup keydown keyup", "", resetTimeout);
                //$(window).resize(resetTimeout);

                // Start fresh by reseting timeout.
                resetTimeout();
            }
        };
    }
})(jQuery);