﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Acd.Web.Public.Filters
{
    public class CustomRequireHttpsAttribute : RequireHttpsAttribute
    {
        private string[] _ignoreActions;

        public CustomRequireHttpsAttribute()
        {
            _ignoreActions = new string[] { };
        }

        public CustomRequireHttpsAttribute(params string[] ignoreActions)
        {
            _ignoreActions = ignoreActions;
            if (_ignoreActions == null)
            {
                _ignoreActions = new string[] { };
            }
        }

        protected override void HandleNonHttpsRequest(AuthorizationContext filterContext)
        {
            var requireHttps = N2.Content.Traverse.StartPage.GetDetail<bool>("RequireHttps", false);
            if (requireHttps)
            {
                var actionName = string.Format("{0}", filterContext.RouteData.Values["action"]);
                requireHttps = !_ignoreActions.Any(x => string.Compare(x, actionName, true) == 0);
            }

            if (requireHttps)
            {
                base.HandleNonHttpsRequest(filterContext);
            }
        }
    }
}