﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using N2.Engine.Globalization;
using N2.Engine;
using System.Security.Principal;
using N2.Definitions;
using System.Data.Linq;
using N2.Web;
using N2;
using System.Linq;
using N2.Web.UI;

namespace Acd.Web.Public.Services
{
    public class GoogleSiteMapHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Entry point for the handler.
        /// </summary>
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ClearHeaders();
            context.Response.ClearContent();

            context.Response.ContentType = "text/xml";

            context.Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            WriteSiteMap(context);

            SetOutputCache(context);
        }

        protected virtual void SetOutputCache(HttpContext context)
        {
            context.TrySetCompressionFilter();
            context.Response.SetOutputCache(Utility.CurrentTime().AddDays(1));
            context.Response.AddCacheDependency(new ContentCacheDependency(Engine.Persister));
        }

        public IEngine Engine { get { return Context.Current; } }

        public void WriteSiteMap(HttpContext context)
        {
            using (XmlWriter writer = CreateXmlWriter(context))
            {
                writer.WriteStartDocument();

                // <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
                writer.WriteStartElement("urlset", "http://www.sitemaps.org/schemas/sitemap/0.9");

                string baseUrl = GetBaseUrl(context);
                var root = Engine.UrlParser.FindPath(context.Request.Url).StopItem;
                WriteItem(writer, baseUrl, root);
                var descendants = GetDescendants(root);
                foreach (var item in descendants)
                {
                    WriteItem(writer, baseUrl, item);
                }

                // <urlset>
                writer.WriteEndElement();
            }
        }

        protected virtual XmlWriter CreateXmlWriter(HttpContext context)
        {
            XmlWriterSettings settings = new XmlWriterSettings { OmitXmlDeclaration = true, Encoding = Encoding.UTF8 };
            return XmlWriter.Create(context.Response.Output, settings);
        }

        protected virtual void WriteItem(XmlWriter writer, string baseUrl, ContentItem item)
        {
            // <url>
            var url = item.Url;
            if (string.Compare(item.TemplateKey, "Redirect", true) == 0)
            {
                var redirectUrl = item.GetDetail<string>("Destination", null);
                if (!string.IsNullOrWhiteSpace(redirectUrl))
                {
                    url = redirectUrl;
                }
            }

            if (!url.StartsWith("http"))
            {
                writer.WriteStartElement("url");

                writer.WriteElementString("loc", baseUrl + url);
                writer.WriteElementString("lastmod", item.Published.GetValueOrDefault().ToString("yyyy-MM-dd")); // Google doesn't like IS0 8601/W3C 
                writer.WriteElementString("changefreq", "weekly"); // TODO make this a setting
                writer.WriteElementString("priority", "0"); // TODO make this a setting

                // </url>
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Get base URL to publish for items in the sitemap.
        /// </summary>
        public virtual string GetBaseUrl(HttpContext context)
        {
            return "http://" + context.Request.Url.Authority;
        }

        public virtual IEnumerable<Acd.Web.Public.Models.ContentPage> GetDescendants(ContentItem parent)
        {
            var children = parent.Children.FindPages().Cast<Acd.Web.Public.Models.ContentPage>();
            foreach (var child in children)
            {
                var excluded = child.ExcludeFromSiteMap;

                if (!excluded
                    && (child is ILanguage
                    || child is IRedirect
                    //|| string.Compare(child.TemplateKey, "Redirect", true) == 0
                    || child is ISystemNode
                    || !child.Visible
                    || !child.IsAuthorized(new GenericPrincipal(new GenericIdentity(""), null))))
                {
                    excluded = true;
                }

                if (!excluded)
                {
                    yield return child as Acd.Web.Public.Models.ContentPage;
                }

                foreach (var descendant in GetDescendants(child))
                {
                    yield return descendant;
                }
            }
        }
    }
}