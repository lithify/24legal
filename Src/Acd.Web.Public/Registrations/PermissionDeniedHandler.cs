﻿using Acd.Web.Public.Models;
using N2;
using N2.Engine;
using N2.Plugin;
using N2.Security;
using N2.Web;

namespace Acd.Web.Public.Registrations
{
    [Service]
    public class PermissionDeniedHandler : IAutoStart
    {
        private readonly ISecurityEnforcer _securityEnforcer;
        private readonly IUrlParser _parser;
        private readonly IWebContext _context;

        public PermissionDeniedHandler(ISecurityEnforcer securityEnforcer, IUrlParser parser, IWebContext context)
        {
            _securityEnforcer = securityEnforcer;
            _parser = parser;
            _context = context;
        }

        void securityEnforcer_AuthorizationFailed(object sender, CancellableItemEventArgs e)
        {
            var startPage = N2.Content.Traverse.StartPage as StartPage;
            if (startPage != null && startPage.LoginPage != null)
            {
                e.Cancel = true;
                _context.HttpContext.Response.Redirect(Url.Parse(startPage.LoginPage).AppendQuery("returnUrl", _context.Url.LocalUrl));
            }
        }

        #region IStartable Members

        public void Start()
        {
            _securityEnforcer.AuthorizationFailed += securityEnforcer_AuthorizationFailed;
        }

        public void Stop()
        {
            _securityEnforcer.AuthorizationFailed -= securityEnforcer_AuthorizationFailed;
        }

        #endregion
    }
}