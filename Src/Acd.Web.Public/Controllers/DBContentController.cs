﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Acd.Web.Public.Models;
using N2.Web.Mvc;

namespace Acd.Web.Public.Controllers
{
    public abstract class DBContentController<T> : ContentController<T> where T : N2.ContentItem
    {
        private ACDDataContext _dataContext;

        protected ACDDataContext DataContext
        {
            get
            {
                if (_dataContext == null)
                {
                    _dataContext = new ACDDataContext();
                }
                return _dataContext;
            }
        }

        #region Dispose
        ~DBContentController()
        {
            Dispose();
        }

        protected override void Dispose(bool disposing)
        {
            if (_dataContext != null)
            {
                _dataContext.Dispose();
                _dataContext = null;
            }

            base.Dispose(disposing);
        }
        #endregion
    }

}