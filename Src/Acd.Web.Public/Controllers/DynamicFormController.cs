﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Acd.Web.Public.Models;
using N2.Details;
using N2.Web;
using N2.Web.Mail;
using N2.Web.Mvc;
using Recaptcha.Web;
using Recaptcha.Web.Mvc;
using System.IO;
using System.Collections.Specialized;
using System.Web.UI.WebControls;

namespace Acd.Web.Public.Controllers
{
    [Controls(typeof(DynamicForm))]
    public class DynamicFormController : ContentController<DynamicForm>
    {
        private readonly IMailSender _mailSender;
        private Dictionary<string, string> _fieldValuesByKey;
        private Dictionary<string, string> _fieldValuesByName;


        public DynamicFormController(IMailSender mailSender)
        {
            _mailSender = mailSender;
        }

        public override ActionResult Index()
        {
            return PartialView(CurrentItem);
        }

        [HttpPost]
        public ActionResult Submit(FormCollection form)
        {
            if (CurrentItem.ShowReCaptcha)
            {
                var recaptchaHelper = this.GetRecaptchaVerificationHelper();
                if (String.IsNullOrEmpty(recaptchaHelper.Response)
                    || recaptchaHelper.VerifyRecaptchaResponse() != RecaptchaVerificationResult.Success)
                {
                    TempData["recaptcha_error"] = "Incorrect. Try again.";
                    ModelState.AddModelError("recaptcha_error", "Incorrect. Try again.");
                }
            }

            _fieldValuesByKey = new Dictionary<string, string>();
            _fieldValuesByName = new Dictionary<string, string>();

            var sb = new StringBuilder();
            GetFields(CurrentItem.FormFields, form, sb);

            #region Invalid
            if (!ModelState.IsValid)
            {
                return Redirect(CurrentPage.Url);
            }
            #endregion

            #region Email
            sb.Append("<p> </p><p> </p><p> </p><p> </p>");
            sb.AppendFormat("<p>{0}</p>", Request.UserHostName);
            sb.AppendFormat("<p>{0:yyyy/MM/dd HH:mm:ss}</p>", DateTime.Now);

            if (!string.IsNullOrWhiteSpace(CurrentItem.AdminEmailFrom))
            {
                #region Admin Email
                if (!string.IsNullOrWhiteSpace(CurrentItem.AdminEmailTo) &&
                    !string.IsNullOrWhiteSpace(CurrentItem.AdminEmailBody))
                {
                    var body = CurrentItem.AdminEmailBody;
                    var replacements = new ListDictionary();
                    replacements.Add("{{form-details}}", sb.ToString());
                    SendEmail(CurrentItem.AdminEmailFrom, CurrentItem.AdminEmailTo,
                        CurrentItem.AdminEmailSubject, body, replacements);
                }
                #endregion

                #region Confirm Email
                if (!string.IsNullOrWhiteSpace(Email) &&
                    !string.IsNullOrWhiteSpace(CurrentItem.ConfirmEmailBody))
                {
                    var body = CurrentItem.ConfirmEmailBody;
                    var replacements = new ListDictionary();
                    replacements.Add("{{first-name}}", FirstName);
                    replacements.Add("{{last-name}}", LastName);
                    replacements.Add("{{email}}", Email);

                    SendEmail(CurrentItem.AdminEmailFrom, Email,
                        CurrentItem.ConfirmEmailSubject, body, replacements);
                }
                #endregion
            }
            #endregion

            TempData.Clear();

            var returnUrl = CurrentItem.SubmitPage;
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                returnUrl = "/";
            }
            return Redirect(returnUrl);
        }

        private void SendEmail(string from, string to, string subject, string body, ListDictionary replacements)
        {
            body = GetHtmlEmail(body, subject);

            var md = new MailDefinition()
            {
                From = from,
                Subject = subject,
                IsBodyHtml = true,
            };

            var msg = md.CreateMailMessage(to, replacements, body, new System.Web.UI.Control());

            _mailSender.Send(msg);
        }

        private static string GetHtmlEmail(string emailTemplate, string subject)
        {
            return Resources.EmailBaseTemplate
                .Replace("{{Subject}}", subject)
                .Replace("{{Body}}", emailTemplate);
        }

        private void GetFields(IEnumerable<DynamicFormElementBase> fields,
            FormCollection form, StringBuilder sb)
        {
            foreach (var element in fields)
            {
                if (element.HasFormValue)
                {
                    if (element is DynamicFormFile)
                    {
                        string filename = null;
                        var value = SaveFile(element, out filename);

                        sb.AppendFormat("<p>{0}: <a href='{2}://{3}{4}'><strong>{1}</strong></a></p>",
                            element.Title, filename, Request.Url.Scheme, Request.Url.Authority, value);

                        _fieldValuesByName[element.Name] = value;
                        if (!string.IsNullOrWhiteSpace(element.FieldKey))
                        {
                            _fieldValuesByKey[element.FieldKey] = value;
                        }
                    }
                    else
                    {
                        var value = element.GetValue(form);
                        TempData[string.Format("{0}_value", element.ElementName)] = value;
                        sb.AppendFormat("<p>{0}: <strong>{1}</strong></p>",
                            element.Title, value);

                        _fieldValuesByName[element.Name] = value;
                        if (!string.IsNullOrWhiteSpace(element.FieldKey))
                        {
                            _fieldValuesByKey[element.FieldKey] = value;
                        }
                    }
                }
                //else if (element is DynamicFormContainerBase)
                //{
                //    if ((element as DynamicFormContainerBase).ShowTitle)
                //    {
                //        sb.AppendFormat("<p><strong>{0}</strong></p>", element.Title);
                //    }
                //}
                else
                {
                    sb.AppendFormat("<p><strong>{0}</strong></p>", element.Title);
                }

                //if (element is DynamicFormContainerBase)
                //{
                //    var container = element as DynamicFormContainerBase;
                //    GetFields(container.FormFields, form, sb);
                //}
            }
        }

        protected string FirstName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(CurrentItem.FirstNameKey) &&
                    _fieldValuesByKey.ContainsKey(CurrentItem.FirstNameKey))
                {
                    return _fieldValuesByKey[CurrentItem.FirstNameKey];
                }
                return null;
            }
        }

        protected string LastName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(CurrentItem.LastNameKey) &&
                    _fieldValuesByKey.ContainsKey(CurrentItem.LastNameKey))
                {
                    return _fieldValuesByKey[CurrentItem.LastNameKey];
                }
                return null;
            }
        }

        protected string Email
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(CurrentItem.EmailKey) &&
                    _fieldValuesByKey.ContainsKey(CurrentItem.EmailKey))
                {
                    return _fieldValuesByKey[CurrentItem.EmailKey];
                }
                return null;
            }
        }

        protected string SaveFile(DynamicFormElementBase element, out string displayName)
        {
            string url = string.Empty;
            displayName = string.Empty;
            if (element is DynamicFormFile)
            {
                var root = string.Format("/upload/form_{0}", CurrentItem.ID);
                if (!Directory.Exists(Server.MapPath(root)))
                {
                    Directory.CreateDirectory(Server.MapPath(root));
                }

                if (Request.Files.AllKeys.Contains(element.ElementName))
                {
                    var file = Request.Files[element.ElementName];
                    var filename = file.FileName;
                    if (filename.Contains("\\"))
                    {
                        filename = filename.Substring(filename.LastIndexOf("\\") + 1);
                    }
                    displayName = filename;
                    if (filename.Contains("."))
                    {
                        var index = filename.LastIndexOf(".");
                        var fn = filename.Substring(0, index);
                        var ext = filename.Substring(index + 1);
                        filename = string.Format("{0}_{1}.{2}", fn, Session.SessionID, ext);
                    }
                    else
                    {
                        filename = string.Format("{0}_{1}", filename, Session.SessionID);
                    }
                    url = string.Format("{0}/{1}", root, filename);
                    try
                    {
                        file.SaveAs(Server.MapPath(url));
                    }
                    catch
                    {
                        url = null;
                    }
                }

            }
            return url;
        }
    }
}