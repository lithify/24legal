using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI.WebControls;
using Acd.Web.Public.Filters;
using Acd.Web.Public.Models;
using N2.Web.Mail;

namespace Acd.Web.Public.Controllers
{
    [CustomRequireHttps("Status", "Logout")]
    public class MembershipController : Controller
    {
        public MembershipController(IFormsAuthenticationService formsService, IMembershipService membershipService, IMailSender mailSender, IOnlineFormService onlineFormService)
        {
            FormsService = formsService;
            MembershipService = membershipService;
            _mailSender = mailSender;
            OnlineFormService = onlineFormService;
            OnlineFormService.DataContext = DataContext;
        }

        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
        private readonly IMailSender _mailSender;
        protected IOnlineFormService OnlineFormService { get; set; }

        private ACDDataContext _dataContext;

        protected ACDDataContext DataContext
        {
            get
            {
                if (_dataContext == null)
                {
                    _dataContext = new ACDDataContext();
                }
                return _dataContext;
            }
        }

        #region Dispose
        ~MembershipController()
        {
            Dispose();
        }

        protected override void Dispose(bool disposing)
        {
            if (_dataContext != null)
            {
                _dataContext.Dispose();
                _dataContext = null;
            }

            base.Dispose(disposing);
        }
        #endregion

        // **************************************
        // URL: /Account/LogOn
        // **************************************

        public ActionResult LogOn(string returnUrl)
        {
            var model = new LogOnModel
            {
                ReturnUrl = returnUrl,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                string username;
                if (MembershipService.ValidateUser(model.Email.Trim(), model.Password, out username))
                {
                    FormsService.SignIn(username, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return Redirect(N2.Content.Traverse.StartPage.Url);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // **************************************
        // URL: /Account/LogOff
        // **************************************

        public ActionResult LogOff()
        {
            FormsService.SignOut();

            return Redirect(N2.Content.Traverse.StartPage.Url);
        }

        // **************************************
        // URL: /Account/Ping
        // **************************************
        public JsonResult Ping()
        {
            return Json(new {timeout = FormsAuthentication.Timeout.TotalMilliseconds}, JsonRequestBehavior.AllowGet);
        }

        // **************************************
        // URL: /Account/Register
        // **************************************

        public ActionResult Register(string returnUrl)
        {
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model, string returnUrl)
        {
            if (ModelState.IsValid && Session["registering"] == null)
            {
                Session["registering"] = true;
                // Attempt to register the user
                MembershipCreateStatus createStatus = MembershipService.CreateUser(model.Email, model.Password, model.Email);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    Roles.AddUserToRole(model.Email, "Members");
                    FormsService.SignIn(model.Email, false /* createPersistentCookie */);
                    if (string.IsNullOrWhiteSpace(returnUrl))
                    {
                        returnUrl = N2.Content.Traverse.StartPage.Url;
                    }
                    Session.Remove("registering");
                    return Redirect(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            Session.Remove("registering");
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePassword
        // **************************************

        [Authorize]
        public ActionResult ChangePassword()
        {
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePasswordSuccess
        // **************************************

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        public ActionResult Status()
        {
            var model = OnlineFormService.GetStep1Model();

            return PartialView(null, model.Name);
        }

        public ActionResult Logout()
        {
            return PartialView();
        }

        #region Reset password
        public ActionResult ForgotPassword()
        {
            ViewBag.Email = string.Empty;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(string email)
        {
            email = string.Format("{0}", email).Trim();
            var regex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            if (string.IsNullOrWhiteSpace(email))
            {
                ModelState.AddModelError("email", "This field is required.");
            }
            else if (!regex.IsMatch(email))
            {
                ModelState.AddModelError("email", "Please enter a valid email address.");
            }

            if (ModelState.IsValid)
            {
                var user = MembershipService.GetUser(email);
                if (user != null)
                {
                    long timestamp = 0;
                    var code = MembershipService.GeneratePasswordResetToken(email, out timestamp);
                    var callbackUrl = Url.Action("ResetPassword", "Membership", new { code = code, t = timestamp }, protocol: Request.Url.Scheme);

                    SendForgotPasswordEmail(user.Email, callbackUrl);

                    return RedirectToAction("ForgotPasswordConfirmation", "Membership");
                }

                return RedirectToAction("ForgotPasswordFailed", "Membership");
            }

            ViewBag.Email = email;
            return View();
        }

        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        public ActionResult ForgotPasswordFailed()
        {
            return View();
        }

        public ActionResult ResetPassword(string code, long? t)
        {
            if (code == null || t == null)
            {
                return RedirectToAction("ResetPasswordFailed", "Account");
            }
            var model = new ResetPasswordViewModel()
            {
                Code = code,
                Timestamp = t ?? 0,
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (string.Compare(model.Password, model.ConfirmPassword, false) != 0)
            {
                ModelState.AddModelError("Password", "The new password and confirmation password do not match.");
            }

            if (ModelState.IsValid)
            {
                var email = string.Format("{0}", model.Email).Trim();
                var user = MembershipService.GetUser(model.Email);
                if (user != null && MembershipService.ResetPassword(email, model.Code, model.Timestamp, model.Password))
                {
                    SendResetPasswordEmail(user.Email);

                    return RedirectToAction("ResetPasswordConfirmation", "Membership");
                }
                return RedirectToAction("ResetPasswordFailed", "Membership");
            }

            return View(model);
        }

        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        public ActionResult ResetPasswordFailed()
        {
            return View();
        }

        public void SendForgotPasswordEmail(string email, string url)
        {
            var startPage = N2.Content.Traverse.StartPage as StartPage;
            var from = startPage.ForgotPasswordMailFrom;
            var to = email;
            var subject = startPage.ForgotPasswordMailSubject;
            var body = startPage.ForgotPasswordMailBody;
            if (!string.IsNullOrWhiteSpace(from)
                && !string.IsNullOrWhiteSpace(to)
                && !string.IsNullOrWhiteSpace(body))
            {
                body = GetHtmlEmail(body, subject);
                var replacements = new ListDictionary
                {
                    {"{{url}}", url},
                };

                SendEmail(from, to, subject, body, replacements);
            }
        }

        public void SendResetPasswordEmail(string email)
        {
            var startPage = N2.Content.Traverse.StartPage as StartPage;
            var from = startPage.ForgotPasswordMailFrom;
            var to = email;
            var subject = startPage.ResetPasswordMailSubject;
            var body = startPage.ResetPasswordMailBody;
            if (!string.IsNullOrWhiteSpace(from)
                && !string.IsNullOrWhiteSpace(to)
                && !string.IsNullOrWhiteSpace(body))
            {
                body = GetHtmlEmail(body, subject);

                SendEmail(from, to, subject, body, new ListDictionary());
            }
        }

        private void SendEmail(string from, string to, string subject, string body, ListDictionary replacements)
        {
            body = GetHtmlEmail(body, subject);

            var md = new MailDefinition()
            {
                From = from,
                Subject = subject,
                IsBodyHtml = true,
            };

            var msg = md.CreateMailMessage(to, replacements, body, new System.Web.UI.Control());

            _mailSender.Send(msg);
        }

        private static string GetHtmlEmail(string emailTemplate, string subject)
        {
            return Resources.EmailBaseTemplate
                .Replace("{{Subject}}", subject)
                .Replace("{{Body}}", emailTemplate);
        }
        #endregion
    }
}
