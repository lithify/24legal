﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Acd.Web.Public.Models;
using N2.Web;

namespace Acd.Web.Public.Controllers
{
    [Controls(typeof(OnlineFormPage))]
    public class OnlineFormController : OnlineFormBaseController<OnlineFormPage>
    {
        protected IOnlineFormService OnlineFormService { get; set; }

        public OnlineFormController(IOnlineFormService onlineFormService)
        {
            OnlineFormService = onlineFormService;
            OnlineFormService.DataContext = DataContext;
            OnlineFormService.ModelState = ModelState;
        }

        public override ActionResult Index()
        {
            Response.Cookies.Add(new HttpCookie("hide-form-alert", "true"));

            return RedirectToAction("Progress");
        }

        #region Intro
        public ActionResult Intro()
        {
            var model = new OnlineFormModel();
            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        [HttpPost]
        public ActionResult Intro(OnlineFormModel model)
        {
            return RedirectToAction("Step1");
        }
        #endregion

        #region Step 1
        public ActionResult Step1()
        {
            var model = OnlineFormService.GetStep1Model();
            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        [HttpPost]
        public ActionResult Step1(OnlineFormStep1Model model)
        {
            if (OnlineFormService.SaveStep1(model))
            {
                if (model.GoNext)
                {
                    return RedirectToAction("Step2A");
                }
                else
                {
                    return RedirectToAction("Intro");
                }
            }

            model.OnlineFormPage = CurrentItem;
            return View(model);
        }
        #endregion

        #region Step 2A
        public ActionResult Step2A()
        {
            var model = OnlineFormService.GetStep2AModel();
            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        [HttpPost]
        public ActionResult Step2A(OnlineFormStep2AModel model)
        {
            if (model.NotCompleteStep2a)
            {
                model.NotAppointMaker2 = true;
                model.NotAppointMaker3 = true;
            }

            if (model.NotAppointMaker2)
            {
                model.NotAppointMaker3 = true;
            }

            if (OnlineFormService.SaveStep2A(model))
            {
                if (model.GoNext)
                {
                    return RedirectToAction("Step2B");
                }
                else
                {
                    return RedirectToAction("Step1");
                }
            }

            model.OnlineFormPage = CurrentItem;
            return View(model);
        }
        #endregion

        #region Step 2B
        public ActionResult Step2B()
        {
            var model = OnlineFormService.GetStep2BModel();
            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        [HttpPost]
        public ActionResult Step2B(OnlineFormStep2BModel model)
        {
            if (OnlineFormService.SaveStep2B(model))
            {
                if (model.GoNext)
                {
                    return RedirectToAction("Step3");
                }
                else
                {
                    return RedirectToAction("Step2A");
                }
            }

            model.OnlineFormPage = CurrentItem;
            return View(model);
        }
        #endregion

        #region Step 3
        public ActionResult Step3()
        {
            var model = OnlineFormService.GetStep3Model();
            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        [HttpPost]
        public ActionResult Step3(OnlineFormStep3Model model)
        {
            if (OnlineFormService.SaveStep3(model))
            {
                if (model.GoNext)
                {
                    return RedirectToAction("Step4");
                }
                else
                {
                    return RedirectToAction("Step2B");
                }
            }

            model.OnlineFormPage = CurrentItem;
            return View(model);
        }
        #endregion

        #region Step 4
        public ActionResult Step4()
        {
            var model = OnlineFormService.GetStep4Model();
            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        [HttpPost]
        public ActionResult Step4(OnlineFormStep4Model model)
        {
            if (OnlineFormService.SaveStep4(model))
            {
                if (model.GoNext)
                {
                    return RedirectToAction("Step5");
                }
                else
                {
                    return RedirectToAction("Step3");
                }
            }

            model.OnlineFormPage = CurrentItem;
            return View(model);
        }
        #endregion

        #region Step 5
        public ActionResult Step5()
        {
            var model = OnlineFormService.GetStep5Model();
            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        [HttpPost]
        public ActionResult Step5(OnlineFormStep5Model model)
        {
            if (OnlineFormService.SaveStep5(model))
            {
                if (model.GoNext)
                {
                    return RedirectToAction("Step6");
                }
                else
                {
                    return RedirectToAction("Step4");
                }
            }

            model.OnlineFormPage = CurrentItem;
            return View(model);
        }
        #endregion

        #region Step 6
        public ActionResult Step6()
        {
            var model = OnlineFormService.GetStep6Model();
            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        [HttpPost]
        public ActionResult Step6(OnlineFormStep6Model model)
        {
            if (OnlineFormService.SaveStep6(model))
            {
                if (model.GoNext)
                {
                    return RedirectToAction("Review");
                }
                else
                {
                    return RedirectToAction("Step5");
                }
            }

            model.OnlineFormPage = CurrentItem;
            return View(model);
        }
        #endregion

        #region Review
        public ActionResult Review()
        {
            var model = OnlineFormService.GetReviewModel();
            if (model.OnlineForm == null ||
                model.OnlineForm.Status < 1)
            {
                return RedirectToAction("Index");
            }
            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        [HttpPost]
        public ActionResult Review(OnlineFormReviewModel model)
        {
            if (!model.Reviewed)
            {
                ModelState.AddModelError("Reviewed", "Please check the box.");
            }

            if (ModelState.IsValid)
            {
                return RedirectToAction("Complete");
            }

            var reviewed = model.Reviewed;
            model = OnlineFormService.GetReviewModel();
            model.OnlineFormPage = CurrentItem;
            model.Reviewed = reviewed;

            return View(model);
        }
        #endregion

        #region Complete
        public ActionResult Complete()
        {
            if (!OnlineFormService.ValidateComplete())
            {
                return RedirectToAction("Review");
            }

            var model = OnlineFormService.Complete(
                CurrentItem.AdminMailFrom, CurrentItem.AdminMailTo, CurrentItem.AdminMailSubject, CurrentItem.AdminMailBody,
                CurrentItem.ConfirmMailSubject, CurrentItem.ConfirmMailBody);
            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        public FileResult Download()
        {
            var onlineForm = OnlineFormService.GetLatestCompletedOnlineForm();
            if (onlineForm == null || string.IsNullOrWhiteSpace(onlineForm.PDFSheet))
            {
                return null;
            }

            var url = onlineForm.PDFSheet;
            var filename = Server.MapPath(url);
            return File(filename, "application/pdf", "AdvanceCareDirectiveForm.pdf");
        }
        #endregion

        #region Progress
        public ActionResult Progress()
        {
            var model = OnlineFormService.GetModel();
            if (model.OnlineForm == null ||
                model.OnlineForm.Status == 0)
            {
                return RedirectToAction("Intro");
            }

            model.OnlineFormPage = CurrentItem;

            return View(model);
        }

        public ActionResult ProgressHeader(int? step)
        {
            var model = OnlineFormService.GetModel();
            if (model.OnlineForm == null)
            {
                model.OnlineForm = new OnlineForm();
            }
            model.OnlineFormPage = CurrentItem;
            model.CurrentStep = step ?? 0;
            if (model.OnlineForm.Status < step)
            {
                model.OnlineForm.Status = step ?? 0;
            }

            return PartialView(model);
        }
        #endregion

        public ActionResult IncompleteFormAlert()
        {
            var completed = false;
            if (Request.Cookies["hide-form-alert"] == null ||
                Request.Cookies["hide-form-alert"].Value != "true")
            {

                var model = OnlineFormService.GetModel();
                if (User.Identity.IsAuthenticated)
                {
                    completed = model.OnlineForm == null ? true : model.OnlineForm.Status == 100;
                }
                else
                {
                    completed = true;
                }
            }

            if (!User.Identity.IsAuthenticated)
            {
                completed = true;
            }

            return Json(new
            {
                completed = completed,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HideFormAlert()
        {
            Response.Cookies.Add(new HttpCookie("hide-form-alert", "true"));

            return Json(new
            {
                success = true,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Export()
        {
            var stream = new MemoryStream();
            OnlineFormService.ExportCsv(stream);
            stream.Position = 0;

            return File(stream, "text/csv", String.Format("online-form-export-{0:yyyyMMddHHmmss}.csv", DateTime.Now));
        }
    }
}