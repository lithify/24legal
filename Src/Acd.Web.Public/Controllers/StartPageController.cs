﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using Acd.Web.Public.Models;
using N2;
using N2.Definitions;
using N2.Engine;
using N2.Engine.Globalization;
using N2.Persistence;
using N2.Persistence.Search;
using N2.Web;
using N2.Web.Mvc;
using N2Content = N2.Content;
namespace Acd.Web.Public.Controllers
{
    /// <summary>
    /// The registration <see cref="Registrations.StartPageRegistration"/> is responsible for
    /// connecting this controller to the start page model.
    /// </summary>
    public class StartPageController : ContentController<StartPage>
    {
        public ActionResult NotFound()
        {
            var closestMatch = Content.Traverse.Path(Request.AppRelativeCurrentExecutionFilePath.Trim('~', '/')).StopItem;

            var startPage = Content.Traverse.ClosestStartPage(closestMatch);
            var urlText = Request.AppRelativeCurrentExecutionFilePath.Trim('~', '/').Replace('/', ' ');
            var similarPages = GetSearchResults(startPage, urlText, 10).ToList();

            ControllerContext.RouteData.ApplyCurrentPath(new PathData(new ContentPage { Parent = startPage }));
            Response.TrySkipIisCustomErrors = true;
            Response.Status = "404 Not Found";

            return View(similarPages);
        }

        [ContentOutputCache]
        public ActionResult SiteMap()
        {
            var start = this.Content.Traverse.StartPage;
            var content = Tree.From(start)
                .Filters(N2Content.Is.Accessible(),
                    new N2.Collections.PageFilter(),
                    new N2.Collections.PublishedFilter(),
                    new N2.Collections.DelegateFilter(x =>
                    {
                        var destination = x.GetDetail<string>("Destination", null);
                        return string.IsNullOrWhiteSpace(destination);
                    }),
                    new N2.Collections.DelegateFilter(x =>
                    {
                        var excluded = x.GetDetail("ExcludeFromSiteMap", false);
                        return !excluded;
                    }))
                .ExcludeRoot(true).ToString();
            return Content("<ul>"
                + "<li>" + Link.To(start) + "</li>"
                + content + "</ul>");
        }

        public ActionResult Search(string q)
        {
            if (string.IsNullOrWhiteSpace(q))
                return Content("<ul><li>A search term is required</li></ul>");

            var hits = GetSearchResults(CurrentPage ?? this.Content.Traverse.StartPage, q, 50);

            var results = new StringBuilder();
            HashSet<ContentPage> appendedPages = new HashSet<ContentPage>();
            foreach (var hit in hits)
            {
                var page = hit as ContentPage;
                if (page == null)
                {
                    // Probably hit a part...
                    page = this.Content.Traverse.ClosestPage(hit) as ContentPage;
                }

                if (page == null || appendedPages.Contains(page))
                    continue;

                appendedPages.Add(page);
            }

            foreach (var page in appendedPages.Where(p => p.Title.IndexOf(q, StringComparison.OrdinalIgnoreCase) >= 0).ToList())
            {
                appendedPages.Remove(page);
                AppendPage(results, page);
            }

            foreach (var page in appendedPages.Where(p => p.Summary.IndexOf(q, StringComparison.OrdinalIgnoreCase) >= 0).ToList())
            {
                appendedPages.Remove(page);
                AppendPage(results, page);
            }

            foreach (var page in appendedPages.ToList())
            {
                appendedPages.Remove(page);
                AppendPage(results, page);
            }

            if (results.Length == 0)
                return Content("<ul><li>No hits</li></ul>");

            return Content("<ul>" + results + "</ul>");
        }

        private void AppendPage(StringBuilder results, ContentPage page)
        {
            results.Append("<li>")
                .Append(Link.To(page))
                .Append("</li>");
        }

        private IEnumerable<ContentItem> GetSearchResults(ContentItem root, string text, int take)
        {
            var query = Query.For(text).Below(root).ReadableBy(User, Roles.GetRolesForUser).Except(Query.For(typeof(ISystemNode)));
            var hits = Engine.Resolve<IContentSearcher>().Search(query).Hits.Select(h => h.Content);
            return hits;
        }

        [ContentOutputCache]
        public ActionResult Translations(int id)
        {
            var sb = new StringBuilder();

            var item = Engine.Persister.Get(id);
            var lg = Engine.Resolve<LanguageGatewaySelector>().GetLanguageGateway(item);
            var translations = lg.FindTranslations(item);
            foreach (var language in translations)
                sb.Append("<li>").Append(Link.To(language).Text(lg.GetLanguage(language).LanguageTitle)).Append("</li>");

            if (sb.Length == 0)
                return Content("<ul><li>This page is not translated</li></ul>");

            return Content("<ul>" + sb + "</ul>");
        }
    }

    [Service(typeof(IContentSearcher))]
    [Service(typeof(ILightweightSearcher))]
    public class CustomFindingContentSearcher : IContentSearcher, ILightweightSearcher
    {
        private IContentItemRepository repository;
        //IItemFinder finder;

        //public FindingContentSearcher(IItemFinder finder)
        public CustomFindingContentSearcher(IContentItemRepository repository)
        {
            //this.finder = finder;
            this.repository = repository;
        }

        #region ITextSearcher Members

        /// <summary>Searches for items below an ancestor that matches the given query.</summary>
        /// <param name="ancestor">The ancestor below which the results should be found.</param>
        /// <param name="query">The query text.</param>
        /// <param name="skip">A number of items to skip.</param>
        /// <param name="take">A number of items to take.</param>
        /// <returns>An enumeration of items matching the search query.</returns>
        public IEnumerable<ContentItem> Search(string ancestor, string query, int skip, int take, bool? onlyPages, string[] types, out int totalRecords)
        {
            var q = new ParameterCollection();
            if (ancestor != null)
            {
                q.Add(Parameter.Like("AncestralTrail", ancestor + "%"));// & Parameter.IsNotNull("Title"));
            }

            if (!string.IsNullOrEmpty(query))
            {
                var words = query.Split(' ').Where(w => w.Length > 0).Select(w => "%" + w.Trim('*') + "%");
                q.Add(new ParameterCollection(Operator.Or, words.Select(word => (Parameter.Like("Title", word) | Parameter.Like(null, word).Detail()))));
            }

            if (onlyPages.HasValue)
                if (onlyPages.Value)
                    q.Add(Parameter.IsNull("ZoneName"));
                else
                    q.Add(Parameter.IsNotNull("ZoneName"));

            if (types != null)
                q.Add(Parameter.In("class", types));

            totalRecords = (int)repository.Count(q);
            return repository.Find(q.Skip(skip).Take(take));

            //var q = finder.Where.AncestralTrail.Like(ancestor + "%")
            //	.And.Title.IsNull(false);

            //var words = query.Split(' ').Where(w => w.Length > 0).Select(w => "%" + w.Trim('*') + "%");
            //foreach (var word in words)
            //{
            //	q = q.And.OpenBracket()
            //		.Title.Like(word)
            //		.Or.Detail().Like(word)
            //	.CloseBracket();
            //}
            //if (onlyPages.HasValue)
            //	q = q.And.OpenBracket()
            //		.ZoneName.IsNull(onlyPages.Value)
            //		.Or.ZoneName.Eq("")
            //	.CloseBracket();
            //if (types != null)
            //	q = q.And.Property("class").In(types);

            //totalRecords = q.Count();
            //return q.FirstResult(skip).MaxResults(take).Select();
        }

        public Result<ContentItem> Search(Query query)
        {
            var result = new Result<ContentItem>();
            int total;
            result.Hits = Search(query.Ancestor, query.Text, query.SkipHits, query.TakeHits, query.OnlyPages, query.Types, out total)
                .Select(i => new Hit<ContentItem> { Title = i.Title, Url = i.Url, Content = i, Score = (i.Title != null && query.Text != null && i.Title.ToLower().Contains(query.Text.ToLower())) ? 1 : .5 });
            result.Total = total;
            return result;
        }

        #endregion

        Result<LightweightHitData> ISearcher<LightweightHitData>.Search(Query query)
        {
            var result = Search(query);
            return new Result<LightweightHitData>
            {
                Count = result.Count,
                Total = result.Total,
                Hits = result.Hits.Select(h => new Hit<LightweightHitData>
                {
                    Score = h.Score,
                    Title = h.Title,
                    Url = h.Url,
                    Content = new LightweightHitData
                    {
                        ID = h.Content.ID,
                        AlteredPermissions = h.Content.AlteredPermissions,
                        AuthorizedRoles = h.Content.AuthorizedRoles.Any() ? h.Content.AuthorizedRoles.Select(ar => ar.Role).ToArray() : new[] { "Everyone" },
                        State = h.Content.State,
                        Visible = h.Content.Visible,
                        Path = h.Content.Path
                    }
                }).ToList()
            };
        }
    }
}
