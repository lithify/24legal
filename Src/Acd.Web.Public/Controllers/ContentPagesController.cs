﻿using System.Web.Mvc;
using Acd.Web.Public.Models;
using N2.Web;
using N2.Web.Mvc;

namespace Acd.Web.Public.Controllers
{
    [Controls(typeof(ContentPage))]
	public class ContentPagesController : ContentController<ContentPage>
	{
		public override ActionResult Index()
		{
			return View(CurrentItem.TemplateKey, CurrentItem);
		}
	}
}
