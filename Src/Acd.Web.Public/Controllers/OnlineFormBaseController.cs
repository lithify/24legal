﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Acd.Web.Public.Filters;

namespace Acd.Web.Public.Controllers
{
    [CustomRequireHttps("IncompleteFormAlert", "HideFormAlert")]
    public abstract class OnlineFormBaseController<T> : DBContentController<T> where T : N2.ContentItem
    {
    }
}