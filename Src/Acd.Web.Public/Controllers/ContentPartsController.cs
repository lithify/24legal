﻿using System.Web.Mvc;
using Acd.Web.Public.Models;
using N2.Web;
using N2.Web.Mvc;

namespace Acd.Web.Public.Controllers
{
    [Controls(typeof(ContentPart))]
	public class ContentPartsController : ContentController<ContentPart>
	{
		public override ActionResult Index()
		{
			return PartialView(CurrentItem.TemplateKey, CurrentItem);
		}
	}
}
