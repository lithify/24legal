﻿using N2.Details;
using System.Web.UI;
using System.Web.UI.WebControls;
using N2;

namespace Acd.Web.Public.Editors
{
    public class EditableFormEntriesLinkAttribute : AbstractEditableAttribute
    {
        public EditableFormEntriesLinkAttribute()
        {

        }

        public EditableFormEntriesLinkAttribute(string title, string name, int sortOrder)
            : base(title, name, sortOrder)
        {
        }

        public EditableFormEntriesLinkAttribute(string title, int sortOrder)
            : base(title, sortOrder)
        {
        }

        public override bool UpdateItem(ContentItem item, Control editor)
        {
            return true;
        }

        public override void UpdateEditor(ContentItem item, Control editor)
        {
            var link = editor as HyperLink;
            link.NavigateUrl = string.Format("/OnlineForm/Export?page={0}", item.ID);
        }

        protected override Control AddEditor(Control container)
        {
            var link = new HyperLink();
            link.Text = "Export Form Entries";
            link.Target = "_blank";
            link.ID = Name;

            container.Controls.Add(link);
            return link;
        }
    }
}
