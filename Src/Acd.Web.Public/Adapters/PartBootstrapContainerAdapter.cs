﻿using System.IO;
using System.Web.Mvc;
using Acd.Web.Public.Models;
using N2;
using N2.Engine;
using N2.Web.Parts;
using Lithe.N2Web;
using Lithe.N2Web.Models;

namespace Acd.Web.Public.Adapters
{
    /// <summary>
    /// Adapts 
    /// </summary>
    [Adapts(typeof(PartModelBase))]
    public class PartBootstrapContainerAdapter : PartsAdapter
    {
        public override void RenderPart(HtmlHelper html, ContentItem part, TextWriter writer = null)
        {
            var wrap = Defaults.IsContainerWrappable(part.ZoneName) && part.GetDetail("UseContainer", true);

            if (wrap)
            {
                html.ViewContext.Writer.WriteLine("<div class=\"container\">");
            }
            base.RenderPart(html, part);
            if (wrap)
            {
                html.ViewContext.Writer.WriteLine("</div>");
            }
        }
    }
}
