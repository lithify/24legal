﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */


//You can copy this file to any location, modify it and set path to it in web.config
//<ckeditor ckConfigJsPath="Path to Your own config file" overwriteStylesSet="Globaly use custom Stylesset (defined in custom ckConfig file)" overwriteFormatTags="Predefine your own formats" contentsCssPath="Path to your own contents.css" />

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here.
    // For the complete reference:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    config.uiColor = '#F8F8F8';
    config.filebrowserWindowWidth = 300;
    config.filebrowserWindowHeight = 600;
    config.extraPlugins = 'codemirror';
    config.height = 300;
    config.entities = false;
    config.htmlEncodeOutput = false;
    config.allowedContent = true;
    config.pasteFromWordRemoveStyles = false;
    config.codemirror = {
        showFormatButton: true,
        showCommentButton: false,
        showUncommentButton: false
    };

    // extra allowed content for Twitter Bootstrap styles
    config.extraAllowedContent =
		  "table(table,table-bordered,table-condensed,table-striped,table-hover);tr(success,error,warning,info);"
		+ "address;abbr[title];cite(pull-right);dl(dl-horizontal);dt;code;"
		+ "div(alert,alert-error,alert-success,alert-info,container,container-fluid,hero-unit,media,media-body,page-header,row,span1,span2,span3,span4,span5,span6,span7,span8,span9,span10,span11,span12,well,well-large,well-small);"
		+ "p(lead);"
		+ "*(media-heading,visible-phone,visible-tablet,visible-desktop,hidden-phone,hidden-tablet,hidden-desktop,muted,pull-left,pull-right);"
		+ "span(label,label-success,label-warning,label-important,label-info,label-inverse);"
		+ "img[data-src](media-object);"
		+ "a button(btn,btn-primary,btn-info,btn-success,btn-warning,btn-danger,btn-inverse,btn-link,btn-large,btn-small,btn-mini)";

};

// Define one or multiple Stylesets
// You can globaly set the used Styleset in web.config
// or define it with UseStylesSet property within EditableFreeTextArea
// How to build stylesets, refere to http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Styles



CKEDITOR.stylesSet.add('my_styles',
[
/* Block Styles */

	// These styles are already available in the "Format" combo ("format" plugin),
	// so they are not needed here by default. You may enable them to avoid
	// placing the "Format" combo in the toolbar, maintaining the same features.
	/*
	{ name: 'Paragraph',		element: 'p' },
	{ name: 'Heading 1',		element: 'h1' },
	{ name: 'Heading 2',		element: 'h2' },
	{ name: 'Heading 3',		element: 'h3' },
	{ name: 'Heading 4',		element: 'h4' },
	{ name: 'Heading 5',		element: 'h5' },
	{ name: 'Heading 6',		element: 'h6' },
	{ name: 'Preformatted Text',element: 'pre' },
	{ name: 'Address',			element: 'address' },
	*/

	{ name: 'Italic Title', element: 'h2', styles: { 'font-style': 'italic' } },
	{ name: 'Subtitle', element: 'h3', styles: { 'color': '#aaa', 'font-style': 'italic' } },
	{
	    name: 'Special Container',
	    element: 'div',
	    styles: {
	        padding: '5px 10px',
	        background: '#eee',
	        border: '1px solid #ccc'
	    }
	},
    {
        name: 'Feature Box',
        element: 'div',
        attributes: { 'class': 'feature-box' }
    },
        {
            name: 'Download Link',
            element: 'div',
            attributes: { 'class': 'download-link' }
        },
       

        /* Inline Styles */

	    // These are core styles available as toolbar buttons. You may opt enabling
	    // some of them in the Styles combo, removing them from the toolbar.
	    // (This requires the "stylescombo" plugin)
	    /*
	    { name: 'Strong',			element: 'strong', overrides: 'b' },
	    { name: 'Emphasis',			element: 'em'	, overrides: 'i' },
	    { name: 'Underline',		element: 'u' },
	    { name: 'Strikethrough',	element: 'strike' },
	    { name: 'Subscript',		element: 'sub' },
	    { name: 'Superscript',		element: 'sup' },
	    */

        { name: 'Marker', element: 'span', attributes: { 'class': 'marker' } },

	    { name: 'Big', element: 'big' },
	    { name: 'Small', element: 'small' },
	    { name: 'Typewriter', element: 'tt' },

	    { name: 'Computer Code', element: 'code' },
	    { name: 'Keyboard Phrase', element: 'kbd' },
	    { name: 'Sample Text', element: 'samp' },
	    { name: 'Variable', element: 'var' },

	    { name: 'Deleted Text', element: 'del' },
	    { name: 'Inserted Text', element: 'ins' },

	    { name: 'Cited Work', element: 'cite' },
	    { name: 'Inline Quotation', element: 'q' },

	    { name: 'Language: RTL', element: 'span', attributes: { 'dir': 'rtl' } },
	    { name: 'Language: LTR', element: 'span', attributes: { 'dir': 'ltr' } },

	    /* Object Styles */

	    {
	        name: 'Styled image (left)',
	        element: 'img',
	        attributes: { 'class': 'left' }
	    },

	    {
	        name: 'Styled image (right)',
	        element: 'img',
	        attributes: { 'class': 'right' }
	    },

	    {
	        name: 'Compact table',
	        element: 'table',
	        attributes: {
	            cellpadding: '5',
	            cellspacing: '0',
	            border: '1',
	            bordercolor: '#ccc'
	        },
	        styles: {
	            'border-collapse': 'collapse'
	        }
	    },

	    { name: 'Borderless Table', element: 'table', styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' } },
	    { name: 'Square Bulleted List', element: 'ul', styles: { 'list-style-type': 'square' } }



]);