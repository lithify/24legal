$(document).ready(function () {
    var forms = $("form");
    forms.each(function () {
        validate_form($(this));
    });
});

function validate_form(form) {
    var tags = $("[data-validation-required],[data-validation-email]", form);

    $("input[data-validation-required],input[data-validation-email]").blur(function () {
        validate_tag($(this));
    }).keydown(function () {
        clear_validate_status($(this));
    });

    $("input[type=checkbox][data-validation-required],input[type=checkbox][data-validation-email]").click(function () {
        validate_tag($(this));
    });

    form.submit(function () {
        var valid = true;
        tags.each(function () {
            var tag_valid = validate_tag($(this));
            if (valid) {
                valid = tag_valid;
            }
        });

        return valid;
    });
}

function validate_tag(tag) {
    clear_validate_status(tag);

    var valid = true;
    if (valid && tag.attr("data-validation-required")) {
        valid = validate_required_tag(tag);
    }
    if (valid && tag.attr("data-validation-email")) {
        valid = validate_email_tag(tag);
    }

    return valid;
}

function validate_required_tag(tag) {
    var valid = false;
    var type = tag.attr("type");
    if (type == "checkbox" || type == "radio") {
        var name = tag.attr("name");
        var length = $("input[name='" + name + "']:checked").length;
        valid = length > 0;
    }
    else {
        var value = tag.val();
        if (value == undefined || value == null) {
            value = "";
        }
        value = String(value).trim();
        valid = value.length > 0;
    }

    if (valid) {
        add_validate_success(tag);
    } else {
        add_validate_error(tag, tag.attr("data-required-msg"));
    }

    return valid;
}

function validate_email_tag(tag) {
    var valid = true;
    var value = tag.val();
    if (value == undefined || value == null) {
        value = "";
    }
    value = String(value).trim();
    if (value != "") {
        var reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
        valid = reg.test(value);
    }

    if (valid) {
        add_validate_success(tag);
    } else {
        add_validate_error(tag, tag.attr("data-email-msg"));
    }

    return valid;
}

function add_validate_error(tag, msg) {
    var div = tag.parents(".form-group:first");
    if (div.hasClass("has-error")) {
        return;
    } else if (div.hasClass("has-success")) {
        clear_validate_status(tag);
    }

    div.addClass("has-error has-feedback");

    if (tag.attr("data-show-feedback") != "false") {
        //var feedback = $('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
        //div.append(feedback);
    }
    if (!tag.hasClass("error")) {
        tag.addClass("error");
    }
    var tip = $('<label class="error"/>')
        .append(msg);
    div.append(tip);
}

function add_validate_success(tag) {
    var div = tag.parents(".form-group:first");
    if (div.hasClass("has-error") || div.hasClass("has-success")) {
        return;
    }

    div.addClass("has-success has-feedback");
    if (tag.attr("data-show-feedback") != "false") {
        //var feedback = $('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
        //div.append(feedback);
    }
}

function clear_validate_status(tag) {
    var div = tag.parents(".form-group:first");

    tag.removeClass("error");

    var feedback = $("label.error", div);
    feedback.remove();

    div.removeClass("has-success");
    div.removeClass("has-error");
    div.removeClass("has-feedback");
}