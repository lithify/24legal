;

(function ($) {

    // common helper

    $.fn.plupdown = function (options) {
        options = $.extend({
            callback: function () { },
            loadingHtml: "<ul><li class='loading'>Loading...</li></ul>",
            opener: true,
            openerClass: "opener",
            resultsClass: "dropmenu"
        }, options);

        var target = $(this);
        var isForm = this.is("form");

        var close = function (e) {
            if ($(e.target).closest(".inner").any())
                return;
            $(".dropmenu").each(function () {
                $($(this).data("opener")).removeClass("open");
            }).removeData("opener").remove();
            $(document).unbind("click", close);
            $(document).unbind("submit", close);
        };

        var open = function (e) {
            e.preventDefault();
            if ($(this).is(".open"))
                return;
            e.stopPropagation();
            close(e);
            $(this).addClass("open");

            var o = $(this).offset();
            var h = $(this).height();

            var url = isForm ? this.action : this.href;
            var data = isForm ? $(this).serialize() : {};
            var $r = $("<div/>").addClass(options.resultsClass).appendTo(target.parent())
                .html("<a href='javascript:' class='navicon-button x open-btn closer close' aria-hidden='true'><div class='navicon'></div></a><div class='inner'/>")
                .data("opener", this);
            $r.children(".inner")
                .html(options.loadingHtml)
                .load(url, data, options.callback);

            $(document.body).bind("click", close);
            $(document.body).bind("submit", close);
        };

        var $o = this.bind(isForm ? "submit" : "click", open);
        if (options.opener)
            $o.addClass(options.openerClass).append("<span class='arrow'>&nbsp;</span>");

        return this;
    };

    $.fn.any = function () {
        return this.length > 0;
    };

    $.fn.clearonfocus = function () {
        this.each(function () { this.title = this.value; })
            .focus(function () { if (this.value === this.title) this.value = ""; })
            .blur(function () { if (this.value === "") this.value = this.title; });
    };

    $(document).ready(function () {
        // Search

        function highlight(text, container) {
            if (container == undefined || container == null) {
                container = "div.container *";
                return;
            }

            setTimeout(function () {
                var splits = text.split(" ");
                for (var i in splits)
                    $(container).highlight(splits[i]);
            }, 1);
        }

        $("#searchform input").clearonfocus();

        $("#searchform").plupdown({
            callback: function () {
                var value = $("#searchFormInput").val();
                var here = location.href.replace(/#.*/, "");
                $("a", this).each(function (i) {
                    if (i === 0) this.focus();
                    if (this.href === here)
                        $(this).focus().click(function () {
                            $("span.highlight").removeClass("highlight");
                            highlight(value);
                        });
                    this.href += "#q=" + value;
                });
                
                highlight(value, $("a", this));
            },
            opener: false
            
        });

        if (location.hash.match("^#q=")) {
            var text = location.hash.substr(3).replace(/[+]/g, " ");

            $("#searchform input").attr("value", text);
            $("#searchform").submit();

            highlight(text);
        }
    });

})(jQuery);
